/**************************************************************************//**
 * @file
 * @brief Test main file, for testing all .h file functions.
 ****************************************************************************/
#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "game.h"
#include <string>
#include <sstream>

using namespace std;


TEST_CASE("typedefs")
{
    REQUIRE((P1MAN & WHITE));
    REQUIRE((P1KING & WHITE));
    REQUIRE((P2MAN & BLACK));
    REQUIRE((P2KING & BLACK));
    REQUIRE((P1KING & KING));
    REQUIRE((P2KING & KING));
    REQUIRE(!(P1MAN & BLACK));
    REQUIRE(!(P1KING & BLACK));
    REQUIRE(!(P2MAN & WHITE));
    REQUIRE(!(P2KING & WHITE));
    REQUIRE(!(P1MAN & KING));
    REQUIRE(!(P2MAN & KING));
}


TEST_CASE("Player.poll_for_move")
{
    Player p { WHITE };
    istringstream input;
    Move expected;
    Move actual;

    SECTION("Stops at newline.")
    {
        expected.emplace(1, 3);
        expected.emplace(2, 4);
        expected.emplace(3, 5);

        input = istringstream("1 3, 2 4, 3 5,\n 4 4");

        Move actual = p.poll_for_move(input);

        REQUIRE(expected.size() == actual.size());

        for ( int s = expected.size(), i = 0; i < s; i++)
        {
            REQUIRE(expected.front() == actual.front());
            expected.pop();
            actual.pop();
        }
    }
}

TEST_CASE("Board operator<<")
{
    Board board;
    ostringstream output;
    string expected =
        "  1 2 3 4 5 6 7 8 \n"
        "1 0   0   0   0   1\n"
        "2   0   0   0   0 2\n"
        "3 0   0   0   0   3\n"
        "4                 4\n"
        "5                 5\n"
        "6   +   +   +   + 6\n"
        "7 +   +   +   +   7\n"
        "8   +   +   +   + 8\n"
        "  1 2 3 4 5 6 7 8 \n";

    output << board;

    REQUIRE(output.str() == expected);
}

TEST_CASE("board.in_bounds")
{
    vector<pair<int, int> > valid = {
        { 1, 1 }, { 8, 8 }, { 1, 8 }, { 8, 1 },
        { 4, 4 }, { 2, 3 }, { 5, 6 }, { 2, 7 }
    };
    vector<pair<int, int> > invalid = {
        { 0, 1 }, {  5, 0 }, { 9, 3  }, { 6, 9  },
        { 0, 9 }, { -3, 3 }, { 5, 12 }, { 2, -7 }
    };
    Board board;
    ostringstream strout;

    auto _cout = cout.rdbuf(strout.rdbuf()); // redirect standard output

    for ( auto pos : valid ) REQUIRE(board.in_bounds(pos));
    for ( auto pos : invalid ) REQUIRE(!board.in_bounds(pos));

    cout.rdbuf(_cout); // return standard output to its previous value
}
