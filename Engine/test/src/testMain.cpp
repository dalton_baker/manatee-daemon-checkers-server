/**************************************************************************//**
 * @file
 * @brief Testing .cpp file for board and game files.
 ****************************************************************************/
#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "player.h"
//#include "game.h"
#include "rules.h"
#include <string>
#include <sstream>

using namespace std;

// TEST_CASE("Game: Test apply_move")
// {
//     Player one, two;
//     Game game(&one, &two);
//     Move move;
//
//     CHECK( !game.apply_move( move ) );
//
//     move.push( make_pair(2, 3) );
//     move.push( make_pair(1, 4) );
//
//     CHECK( game.apply_move( Move() ) );
// }
//
// TEST_CASE("Game: Test is_over")
// {
//     Player one, two;
//     Game game(&one, &two);
//
//     CHECK( !game.is_over() );
// }
//
// TEST_CASE("Game: Test print_board")
// {
//     Player one, two;
//     Game game(&one, &two);
//     ostringstream output;
//     string expected =
//         "  1 2 3 4 5 6 7 8 \n"
//         "1   0   0   0   0 1\n"
//         "2 0   0   0   0   2\n"
//         "3   0   0   0   0 3\n"
//         "4 #   #   #   #   4\n"
//         "5   #   #   #   # 5\n"
//         "6 +   +   +   +   6\n"
//         "7   +   +   +   + 7\n"
//         "8 +   +   +   +   8\n"
//         "  1 2 3 4 5 6 7 8 \n";
//
//     game.print_board(output);
//
//     CHECK(output.str() == expected);
// }
//
// TEST_CASE("Game: Test return_board")
// {
//     Player one1, two1;
//     Game game1(&one1, &two1);
//     Player one2, two2;
//     Game game2(&one2, &two2);
//
//     Board board1    = game1.return_board();
//     Board board2    = game2.return_board();
//
//     CHECK(board1 == board2);
// }
//
// TEST_CASE("Game: Test run")
// {
//     Player one, two;
//     Game game(&one, &two);
//     istringstream istr(
//         "B3 A4\nA6 B5\nH3 G4\nG6 H5\nG4 F5\nE6 G4\nF3 E4\nB7 A6\nG2 F3\nG4 H3\n"
//         "D3 C4\nB5 D3\nE2 C4\nF7 G6\nE4 D5\nC6 E4 G2\nH1 F3\nA6 B5\nC4 A6\n"
//         "A8 B7\nF1 E2\nH3 G2\nF3 E4\nH5 G4\nC2 B3\nG2 F1\nB1 C2\nF1 D3 B1\n"
//         "A4 B5\nB7 C6\nD1 E2\nC6 A4 C2\nE4 F5\nG6 E4\nA2 B3\nD7 E6\nB3 A4\n"
//         "C8 D7\nA6 B7\nH7 G6\nB7 A8\nD7 C6\nE2 D3\nG8 H7\nD3 F5 D7\nG4 H3\n"
//         "D7 C8\nC2 D1\nA4 B5\nC6 A4\nC8 D7\nE8 C6\nA8 B7\nB1 C2\nB7 D5\nC2 D3\n"
//         "D5 E6\nD3 C2\nE6 D5\nC2 D3\nD5 E6\nD3 C2\nE6 D5\nC2 D3\nD5 E6\nD3 C2\n"
//         "E6 D5\nC2 D3\nD5 E4\nD3 F5\n"
//         );
//     auto _cin = cin.rdbuf( istr.rdbuf() ); // redirect standard input
//
//     game.run();
//
//     cin.rdbuf(_cin); // return standard input to its previous value
// }

TEST_CASE("Board: Test is_ functions")
{
    Board testBoard;

    Position p1(2, 3);
    Position p2(1, 6);
    Position empty(1, 4);

    CHECK( testBoard.return_pieces().at(p1).is_player1() );
    CHECK( testBoard.return_pieces().at(p2).is_player2() );
    CHECK( testBoard.is_empty(empty) );

}

TEST_CASE("Rules: Test move to unavailable space")
{
    Board testBoard;
    Move testMoveP1;
    Move testMoveP2;

    testMoveP1.push(make_pair(1, 2));
    testMoveP1.push(make_pair(1, 3));

    Rules testRulesP1( testBoard, P_ONE);
    CHECK(!testRulesP1.find_move(testBoard, testMoveP1));


    testMoveP2.push(make_pair(1, 6));
    testMoveP2.push(make_pair(1, 5));
    Rules testRulesP2( testBoard, P_TWO);
    CHECK(!testRulesP2.find_move(testBoard, testMoveP2));
}

TEST_CASE("Rules: Test move to an available space")
{
    Board testBoard;
    Move testMoveP1;
    Move testMoveP2;

    testMoveP1.push(make_pair(2, 3));
    testMoveP1.push(make_pair(3, 4));

    Rules testRulesP1( testBoard, P_ONE);
    CHECK(testRulesP1.find_move(testBoard, testMoveP1));


    testMoveP2.push(make_pair(1, 6));
    testMoveP2.push(make_pair(2, 5));
    Rules testRulesP2( testBoard, P_TWO);
    CHECK(testRulesP2.find_move(testBoard, testMoveP2));
}

TEST_CASE("Rules: Test jump move")
{
    Board testBoard;
    Move testMoveP1;
    Move badMoveP1;
    Move testMoveP2;
    Move badMoveP2;

    testBoard.move_piece(make_pair(7, 6), make_pair(7, 4));
    testBoard.move_piece(make_pair(2, 3), make_pair(2, 5));



    testMoveP1.push(make_pair(8, 3));
    testMoveP1.push(make_pair(6, 5));

    badMoveP1.push(make_pair(4, 3));
    badMoveP1.push(make_pair(5, 4));

    Rules testRulesP1( testBoard, P_ONE);
    CHECK(!testRulesP1.find_move(testBoard, badMoveP1));
    CHECK(testRulesP1.find_move(testBoard, testMoveP1));


    testMoveP2.push(make_pair(1, 6));
    testMoveP2.push(make_pair(3, 4));

    badMoveP2.push(make_pair(5, 6));
    badMoveP2.push(make_pair(4, 5));

    Rules testRulesP2( testBoard, P_TWO);
    CHECK(!testRulesP2.find_move(testBoard, badMoveP2));
    CHECK(testRulesP2.find_move(testBoard, testMoveP2));
}

TEST_CASE("Rules: Test double jump move")
{
    Board testBoard;
    Move testMoveP1;
    Move badMoveP1;
    Move testMoveP2;
    Move badMoveP2;

    testBoard.move_piece(make_pair(7, 6), make_pair(7, 4));
    testBoard.move_piece(make_pair(2, 3), make_pair(2, 5));
    testBoard.kill_piece(make_pair(5, 2));
    testBoard.kill_piece(make_pair(4, 7));

    testMoveP1.push(make_pair(8, 3));
    testMoveP1.push(make_pair(6, 5));
    testMoveP1.push(make_pair(4, 7));

    badMoveP1.push(make_pair(4, 3));
    badMoveP1.push(make_pair(5, 4));

    Rules testRulesP1( testBoard, P_ONE);
    CHECK(!testRulesP1.find_move(testBoard, badMoveP1));
    CHECK(testRulesP1.find_move(testBoard, testMoveP1));


    testMoveP2.push(make_pair(1, 6));
    testMoveP2.push(make_pair(3, 4));
    testMoveP2.push(make_pair(5, 2));

    badMoveP2.push(make_pair(5, 6));
    badMoveP2.push(make_pair(4, 5));

    Rules testRulesP2( testBoard, P_TWO);
    CHECK(!testRulesP2.find_move(testBoard, badMoveP2));
    CHECK(testRulesP2.find_move(testBoard, testMoveP2));
}

TEST_CASE("Rules: Test tripple jump move")
{
    Board testBoard;
    Move testMove;

    testBoard.move_piece(make_pair(4, 3), make_pair(5, 4));
    testBoard.kill_piece(make_pair(7, 6));
    testBoard.move_piece(make_pair(6, 3), make_pair(7, 6));
    testBoard.kill_piece(make_pair(2, 1));

    testMove.push(make_pair(8, 7));
    testMove.push(make_pair(6, 5));
    testMove.push(make_pair(4, 3));
    testMove.push(make_pair(2, 1));

    Rules testRules( testBoard, P_TWO);
    CHECK(testRules.find_move(testBoard, testMove));
}

TEST_CASE("Rules: Make a king after normal move")
{
    Board testBoard;
    Move testMoveP1;
    Move testMoveP2;
    PieceMap testMap;

    testBoard.kill_piece(make_pair(3, 2));
    testBoard.kill_piece(make_pair(6, 7));
    testBoard.move_piece(make_pair(1, 8), make_pair(3, 2));
    testBoard.move_piece(make_pair(8, 1), make_pair(6, 7));
    testBoard.kill_piece(make_pair(2, 1));
    testBoard.kill_piece(make_pair(7, 8));

    testMoveP2.push(make_pair(3, 2));
    testMoveP2.push(make_pair(2, 1));

    testMoveP1.push(make_pair(6, 7));
    testMoveP1.push(make_pair(7, 8));

    Rules testRulesP2( testBoard, P_TWO);
    CHECK(testRulesP2.find_move(testBoard, testMoveP2));

    CHECK(testBoard.return_pieces().at(make_pair(2, 1)).is_king());
    CHECK(testBoard.return_pieces().at(make_pair(2, 1)).is_player2());

    Rules testRulesP1( testBoard, P_ONE);
    CHECK(testRulesP1.find_move(testBoard, testMoveP1));
    CHECK(testBoard.return_pieces().at(make_pair(7, 8)).is_king());
    CHECK(testBoard.return_pieces().at(make_pair(7, 8)).is_player1());
}

TEST_CASE("Rules: Make a king after jump move")
{
    Board testBoard;
    Move testMove;

    testBoard.move_piece(make_pair(4, 3), make_pair(5, 4));
    testBoard.kill_piece(make_pair(7, 6));
    testBoard.move_piece(make_pair(6, 3), make_pair(7, 6));
    testBoard.kill_piece(make_pair(2, 1));

    testBoard.kill_piece(make_pair(2, 1));

    testMove.push(make_pair(8, 7));
    testMove.push(make_pair(6, 5));
    testMove.push(make_pair(4, 3));
    testMove.push(make_pair(2, 1));

    Rules testRules( testBoard, P_TWO);
    CHECK(testRules.find_move(testBoard, testMove));
    CHECK(testBoard.return_pieces().at(make_pair(2, 1)).is_king());
    CHECK(testBoard.return_pieces().at(make_pair(2, 1)).is_player2());
}

TEST_CASE("Rules: King normal move backwards for player 1")
{
    Board testBoard;
    Move testMove;

    testBoard.king_piece(make_pair(4, 3));
    testBoard.kill_piece(make_pair(3, 2));

    testMove.push(make_pair(4, 3));
    testMove.push(make_pair(3, 2));

    Rules testRules( testBoard, P_ONE);
    CHECK(testRules.find_move(testBoard, testMove));
    CHECK(testBoard.return_pieces().at(make_pair(3, 2)).is_king());
}

TEST_CASE("Rules: King normal move backwards for player 2")
{
    Board testBoard;
    Move testMove;

    testBoard.king_piece(make_pair(3, 6));
    testBoard.kill_piece(make_pair(4, 7));

    testMove.push(make_pair(3, 6));
    testMove.push(make_pair(4, 7));

    Rules testRules( testBoard, P_TWO);
    CHECK(testRules.find_move(testBoard, testMove));
    CHECK(testBoard.return_pieces().at(make_pair(4, 7)).is_king());
}

TEST_CASE("Rules: King jump move backwards for player 1")
{
    Board testBoard;
    Move testMove;

    testBoard.king_piece(make_pair(4, 3));
    testBoard.kill_piece(make_pair(2, 1));
    testBoard.kill_piece(make_pair(3, 2));
    testBoard.move_piece(make_pair(1, 6), make_pair(3, 2));

    testMove.push(make_pair(4, 3));
    testMove.push(make_pair(2, 1));

    Rules testRules( testBoard, P_ONE);
    CHECK(testRules.find_move(testBoard, testMove));
    CHECK(testBoard.return_pieces().at(make_pair(2, 1)).is_king());
    CHECK(testBoard.is_empty(make_pair(3, 2)));
}

TEST_CASE("Rules: King jump move backwards for player 2")
{
    Board testBoard;
    Move testMove;

    testBoard.king_piece(make_pair(3, 6));
    testBoard.kill_piece(make_pair(5, 8));
    testBoard.kill_piece(make_pair(4, 7));
    testBoard.move_piece(make_pair(1, 2), make_pair(4, 7));

    testMove.push(make_pair(3, 6));
    testMove.push(make_pair(5, 8));

    Rules testRules( testBoard, P_TWO);
    CHECK(testRules.find_move(testBoard, testMove));
    CHECK(testBoard.return_pieces().at(make_pair(5, 8)).is_king());
    CHECK(testBoard.is_empty(make_pair(4, 7)));
}

TEST_CASE("Board: Return board map and test is_ functions of Piece class")
{
    Board testBoard;

    PieceMap testMap = testBoard.return_pieces();

    //check all of player 1's pieces
    CHECK(testMap.at(make_pair(2, 1)).is_player1());
    CHECK(testMap.at(make_pair(4, 1)).is_player1());
    CHECK(testMap.at(make_pair(6, 1)).is_player1());
    CHECK(testMap.at(make_pair(8, 1)).is_player1());
    CHECK(testMap.at(make_pair(1, 2)).is_player1());
    CHECK(testMap.at(make_pair(3, 2)).is_player1());
    CHECK(testMap.at(make_pair(5, 2)).is_player1());
    CHECK(testMap.at(make_pair(7, 2)).is_player1());
    CHECK(testMap.at(make_pair(2, 3)).is_player1());
    CHECK(testMap.at(make_pair(4, 3)).is_player1());
    CHECK(testMap.at(make_pair(6, 3)).is_player1());
    CHECK(testMap.at(make_pair(8, 3)).is_player1());

    //check all of player 2's pieces
    CHECK(testMap.at(make_pair(1, 6)).is_player2());
    CHECK(testMap.at(make_pair(3, 6)).is_player2());
    CHECK(testMap.at(make_pair(5, 6)).is_player2());
    CHECK(testMap.at(make_pair(7, 6)).is_player2());
    CHECK(testMap.at(make_pair(2, 7)).is_player2());
    CHECK(testMap.at(make_pair(4, 7)).is_player2());
    CHECK(testMap.at(make_pair(6, 7)).is_player2());
    CHECK(testMap.at(make_pair(8, 7)).is_player2());
    CHECK(testMap.at(make_pair(1, 8)).is_player2());
    CHECK(testMap.at(make_pair(3, 8)).is_player2());
    CHECK(testMap.at(make_pair(5, 8)).is_player2());
    CHECK(testMap.at(make_pair(7, 8)).is_player2());

    //check the is_king function on all pieces
    CHECK(!testMap.at(make_pair(2, 1)).is_king ());
    CHECK(!testMap.at(make_pair(4, 1)).is_king ());
    CHECK(!testMap.at(make_pair(6, 1)).is_king ());
    CHECK(!testMap.at(make_pair(8, 1)).is_king ());
    CHECK(!testMap.at(make_pair(1, 2)).is_king ());
    CHECK(!testMap.at(make_pair(3, 2)).is_king ());
    CHECK(!testMap.at(make_pair(5, 2)).is_king ());
    CHECK(!testMap.at(make_pair(7, 2)).is_king ());
    CHECK(!testMap.at(make_pair(2, 3)).is_king ());
    CHECK(!testMap.at(make_pair(4, 3)).is_king ());
    CHECK(!testMap.at(make_pair(6, 3)).is_king ());
    CHECK(!testMap.at(make_pair(8, 3)).is_king ());
    CHECK(!testMap.at(make_pair(1, 6)).is_king ());
    CHECK(!testMap.at(make_pair(3, 6)).is_king ());
    CHECK(!testMap.at(make_pair(5, 6)).is_king ());
    CHECK(!testMap.at(make_pair(7, 6)).is_king ());
    CHECK(!testMap.at(make_pair(2, 7)).is_king ());
    CHECK(!testMap.at(make_pair(4, 7)).is_king ());
    CHECK(!testMap.at(make_pair(6, 7)).is_king ());
    CHECK(!testMap.at(make_pair(8, 7)).is_king ());
    CHECK(!testMap.at(make_pair(1, 8)).is_king ());
    CHECK(!testMap.at(make_pair(3, 8)).is_king ());
    CHECK(!testMap.at(make_pair(5, 8)).is_king ());
    CHECK(!testMap.at(make_pair(7, 8)).is_king ());
}

TEST_CASE("moveTree: Test adding moves")
{
    Move testMove1;
    Move testMove2;
    Move testMove3;
    MoveTreeCollection moveTrees;

    testMove1.push(make_pair(0, 1));
    testMove1.push(make_pair(2, 3));
    testMove1.push(make_pair(4, 5));
    testMove1.push(make_pair(6, 7));

    testMove2.push(make_pair(1, 3));
    testMove2.push(make_pair(2, 3));
    testMove2.push(make_pair(3, 4));
    testMove2.push(make_pair(4, 4));

    testMove3.push(make_pair(3, 3));
    testMove3.push(make_pair(5, 5));
    testMove3.push(make_pair(4, 4));
    testMove3.push(make_pair(6, 6));

    moveTrees.insert(testMove1);
    moveTrees.insert(testMove2);
    moveTrees.insert(testMove3);

    //make sure the moves trees for the positions exist
    CHECK(moveTrees.hasTree(Position(0, 1)));
    CHECK(moveTrees.hasTree(Position(1, 3)));
    CHECK(moveTrees.hasTree(Position(3, 3)));

    //access children
    CHECK(moveTrees.getTree(Position(0, 1)).hasChild(Position(2,3)));
    CHECK(moveTrees.getTree(Position(0, 1)).getChild(Position(2,3)).hasChild(Position(4,5)));
    CHECK(moveTrees.getTree(Position(0, 1)).getChild(Position(2,3)).getChild(Position(4,5)).hasChild(Position(6,7)));

    //make sure iteration works
    vector<Position> positions;
    for (auto t : moveTrees) //iterate through and get child positions
    {
    	positions.push_back(t.first);
    }
    CHECK(find(positions.begin(), positions.end(), Position(0, 1)) != positions.end());
    CHECK(find(positions.begin(), positions.end(), Position(1, 3)) != positions.end());
    CHECK(find(positions.begin(), positions.end(), Position(3, 3)) != positions.end());

    //create a movetree
    MoveTree tree(Position(1,6));

    tree.addChild(Position(0,5));
    tree.addChild(Position(2,7));

    CHECK(tree.getPos() == Position(1,6)); //root is added
    CHECK(tree.hasChild(Position(0,5))); //root has correct child
    CHECK(tree.hasChild(Position(2,7))); //root has correct child
    CHECK(!tree.getChild(Position(0,5)).hasChildren()); //child is correct

    //add the tree to the collection
    moveTrees.addTree(tree);

    CHECK(moveTrees.hasTree(Position(1,6)));


    //add tree to collection by position
    moveTrees.addTree(Position(8,8));

    CHECK(moveTrees.hasTree(Position(8,8)));
}

TEST_CASE("Rules: Testing special case \"Diamond Pattern\"")
{
    Board testBoard;
    Board dummyBoard;
    Move testMove1;
    Move testMove2;

    //get rid of all but 1 of player 1's pieces
    testBoard.kill_piece(make_pair(4, 1));
    testBoard.kill_piece(make_pair(6, 1));
    testBoard.kill_piece(make_pair(8, 1));
    testBoard.kill_piece(make_pair(1, 2));
    testBoard.kill_piece(make_pair(3, 2));
    testBoard.kill_piece(make_pair(5, 2));
    testBoard.kill_piece(make_pair(7, 2));
    testBoard.kill_piece(make_pair(2, 3));
    testBoard.kill_piece(make_pair(4, 3));
    testBoard.kill_piece(make_pair(6, 3));
    testBoard.kill_piece(make_pair(8, 3));
    testBoard.king_piece(make_pair(2, 1));

    //get rid of all but 4 of player 2's pieces
    testBoard.kill_piece(make_pair(1, 6));
    testBoard.kill_piece(make_pair(3, 6));
    testBoard.kill_piece(make_pair(5, 6));
    testBoard.kill_piece(make_pair(7, 6));
    testBoard.kill_piece(make_pair(2, 7));
    testBoard.kill_piece(make_pair(4, 7));
    testBoard.kill_piece(make_pair(6, 7));
    testBoard.kill_piece(make_pair(8, 7));

    //move the pieces into the same position reported in the bug
    testBoard.move_piece(make_pair(2, 1), make_pair(6, 7));
    testBoard.move_piece(make_pair(1, 8), make_pair(5, 4));
    testBoard.move_piece(make_pair(3, 8), make_pair(7, 4));
    testBoard.move_piece(make_pair(5, 8), make_pair(5, 6));
    testBoard.move_piece(make_pair(7, 8), make_pair(7, 6));

    testMove1.push(make_pair(6, 7));
    testMove1.push(make_pair(8, 5));
    testMove1.push(make_pair(6, 3));
    testMove1.push(make_pair(4, 5));
    testMove1.push(make_pair(6, 7));

    testMove2.push(make_pair(6, 7));
    testMove2.push(make_pair(4, 5));
    testMove2.push(make_pair(6, 3));
    testMove2.push(make_pair(8, 5));
    testMove2.push(make_pair(6, 7));

    Rules testRules( testBoard, P_ONE);

    CHECK(testRules.find_move(dummyBoard, testMove1));
    CHECK(testRules.find_move(dummyBoard, testMove2));
}

TEST_CASE("Rules: Test finding multiple jumps from one piece")
{
    Board testBoard;
    Board dummyBoard;
    Move testMove1;
    Move testMove2;
    Move testMove3;
    Move testMove4;

    //get rid of all but 1 of player 1's pieces
    testBoard.kill_piece(make_pair(4, 1));
    testBoard.kill_piece(make_pair(6, 1));
    testBoard.kill_piece(make_pair(8, 1));
    testBoard.kill_piece(make_pair(1, 2));
    testBoard.kill_piece(make_pair(3, 2));
    testBoard.kill_piece(make_pair(5, 2));
    testBoard.kill_piece(make_pair(7, 2));
    testBoard.kill_piece(make_pair(2, 3));
    testBoard.kill_piece(make_pair(4, 3));
    testBoard.kill_piece(make_pair(6, 3));
    testBoard.kill_piece(make_pair(8, 3));
    testBoard.king_piece(make_pair(2, 1));

    //get rid of all but 4 of player 2's pieces
    testBoard.kill_piece(make_pair(1, 6));
    testBoard.kill_piece(make_pair(3, 6));
    testBoard.kill_piece(make_pair(5, 6));
    testBoard.kill_piece(make_pair(7, 6));
    testBoard.kill_piece(make_pair(2, 7));
    testBoard.kill_piece(make_pair(4, 7));
    testBoard.kill_piece(make_pair(6, 7));
    testBoard.kill_piece(make_pair(8, 7));

    //move the pieces into a possition that will result in 4 jump moves
    testBoard.move_piece(make_pair(2, 1), make_pair(6, 5));
    testBoard.move_piece(make_pair(1, 8), make_pair(5, 4));
    testBoard.move_piece(make_pair(3, 8), make_pair(7, 4));
    testBoard.move_piece(make_pair(5, 8), make_pair(5, 6));
    testBoard.move_piece(make_pair(7, 8), make_pair(7, 6));


    testMove1.push(make_pair(6, 5));
    testMove1.push(make_pair(8, 7));

    testMove2.push(make_pair(6, 5));
    testMove2.push(make_pair(8, 3));

    testMove3.push(make_pair(6, 5));
    testMove3.push(make_pair(4, 7));

    testMove4.push(make_pair(6, 5));
    testMove4.push(make_pair(4, 3));

    Rules testRules( testBoard, P_ONE);
    CHECK(testRules.return_num_moves() == 4);
    CHECK(testRules.find_move(dummyBoard, testMove1));
    CHECK(testRules.find_move(dummyBoard, testMove2));
    CHECK(testRules.find_move(dummyBoard, testMove3));
    CHECK(testRules.find_move(dummyBoard, testMove4));
}
