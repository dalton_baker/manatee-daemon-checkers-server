/**************************************************************************//**
 * @file
 * @brief This is the header for the gameManager.cpp file
 *****************************************************************************/
#ifndef __GAME__H__
#define __GAME__H__

#include <unistd.h>
#include <ctime>
#include <set>
#include "rules.h"
#include "player.h"
#include "ui.h"

using namespace std;

enum InvalidMoveType {NOINVALID, PLAYER1, PLAYER2};
/*!
 * @brief GameManager documentation
 */
class Game
{
protected:
    Board board; /*<! The game's board */
    PlayerID current_player;
    int turns_since_action;
    multiset<Board> board_history;
    Player* player1;
    Player* player2;
    UI* ui;
    Rules rules;
    bool print_console;
    void update_rules();
    void end_turn();
    void update_ui(PlayerID pId);
    InvalidMoveType badMove;

public:
    Game(Player*, Player*, UI*, bool);
    ~Game();

    bool apply_move(Move move);
    int is_over() const;
    void print_board(ostream& out) const;
    Board return_board() const;
    int run(bool);
};

#endif
