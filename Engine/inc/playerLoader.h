#ifndef PLAYERLOADER
#define PLAYERLOADER
#include <iostream>
#include <dirent.h>
#include <errno.h>
#include <dlfcn.h>
#include <sys/types.h>
#include "player.h"

class PlayerLoader
{
   typedef Player *playerCreate_t();
   typedef void playerDestroy_t(Player *);

   struct playerHandle
   {
      Player *ptr;
      playerCreate_t *createFunc;
      playerDestroy_t *destroyFunc;
      void *library;
   };

   std::vector<playerHandle> players;

public:
   void unloadPlayers();
   void loadPlayers(string);
   
   Player* getPlayerObject(std::string name);
};

#endif
