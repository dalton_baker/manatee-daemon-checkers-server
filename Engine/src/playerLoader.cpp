#include "playerLoader.h"

Player* PlayerLoader::getPlayerObject(std::string name)
{
   for (playerHandle &h : players)
      if (h.ptr->Name() == name)
         return h.ptr;
  
   return nullptr;
}

void PlayerLoader::unloadPlayers()
{
   for (playerHandle & t : players)
   {
      t.destroyFunc(t.ptr);
      dlclose(t.library);
   }
}

void PlayerLoader::loadPlayers(std::string dir)
{
   // Pointer to a directory in the file system
   DIR *dp;
   // Pointer to a directory entry
   struct dirent *dirp;
   
   if ((dp = opendir(dir.c_str())) == NULL)
   {
      std::cout << "Error(" << errno << ") opening " << dir << std::endl;
      return;
   }

   while ((dirp = readdir(dp)) != nullptr)
   {
      std::string fileName = std::string(dirp->d_name);
      
      if (fileName.find(".so") != std::string::npos)
      {
         playerHandle newPlayer;
  
         std::string path = dir + "/" + fileName;

         newPlayer.library = dlopen(path.c_str(), RTLD_LAZY);
         if (newPlayer.library == nullptr)
         { 
            std::cout << "unable to open player as shared object " << fileName << " " << dlerror() << std::endl;
            continue;
         }

         dlerror();
         bool failed = false;
 
         newPlayer.createFunc = (playerCreate_t *) dlsym(newPlayer.library, "createPlayer");
         const char *err = dlerror();
         if (err)
         {
            std::cout << "Unable to load createPlayer function: " << err << std::endl;
            failed = true;
         }
         
         dlerror();

         newPlayer.destroyFunc = (playerDestroy_t*) dlsym(newPlayer.library, "destroyPlayer");
     
         err = dlerror();
         if (err)
         {
            std::cout << "Unable to load destroyPlayer function: " << err << std::endl;
            failed = true;
         }
         newPlayer.ptr = newPlayer.createFunc();
         if (newPlayer.ptr == nullptr)
         { 
            std::cout << "Failed to create player" << std::endl;
            failed = true;
         }

         if (failed)
         {
            dlclose(newPlayer.library);
         }
         else
         {
            players.push_back(newPlayer);
         }
      }
   }
   closedir(dp);
}
