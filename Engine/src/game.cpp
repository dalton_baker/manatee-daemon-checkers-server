/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the GameManager class
 *****************************************************************************/
#include <iostream>
#include <algorithm>
#include "game.h"

using namespace std;

// forward declarations
int cmp_kings(const PieceMap& a, const PieceMap& b);
bool has_repeated(const multiset<Board>& states);
bool piece_diff(Board start, Board end);

Game::Game(Player* player_a, Player* player_b, UI* gameUI, bool console)
    : current_player(P_ONE), turns_since_action(0), player1(player_a),
    player2(player_b), ui(gameUI), rules(board, current_player),
    print_console(console), badMove(NOINVALID) {}

Game::~Game() {}

bool Game::apply_move(Move move)
{
    Board previous_board(board);

    update_rules();

    if ( rules.find_move(move) )
    {
        board = rules.get_board(move);

        board_history.insert(previous_board);

        if ( piece_diff(previous_board, board) )
        {
            turns_since_action = 0;
            board_history.clear();
        }
        else
            ++turns_since_action;

        end_turn();

        return true;
    }
    else
        return false;
}

void Game::end_turn()
{
    current_player = ( ( current_player == P_ONE ) ? P_TWO : P_ONE );
}

/****************************************************************************//*
 * @brief Checks if the game is over. Returns an int giving the win state of
            the game. Returns 0 when the game is ongoing.
            Can be used as a boolean check.
 *
 * @returns 0 : the game is not over
 * @returns 1 : player one won the game by capturing all of player two's pieces
 * @returns 2 : player two won the game by capturing all of player one's pieces
 * @returns 3 : player one won the game by pinning player two
 * @returns 4 : player two won the game by pinning player one
 * @returns 5 : the game was a draw by stalling (50 moves without capture/kinging)
 * @returns 6 : the game was a draw due to repeated board state
 ******************************************************************************/
int Game::is_over() const
{
    if (badMove == PLAYER1)
    {
       if (ui != nullptr)
          ui->end_game(P_TWO, player2->Name() + " wins!");
       if ( print_console )
          cout << board;
       cout << player2->Name() + " [Red] wins! -- Invalid Move by Player1\n";
       return 7;
    }

    if (badMove == PLAYER2)
    {
       if (ui != nullptr)
          ui->end_game(P_ONE, player1->Name() + " wins!");
       if ( print_console )
          cout << board;
       cout << player1->Name() + " [Black] wins! -- Invalid Move by Player2\n";
       return 8;
    }

    if ( board.player2_pieces().size() == 0 )
    {
        if ( ui != nullptr )
            ui->end_game(P_ONE, player1->Name() + " wins!");
        if ( print_console )
            cout << board;
        cout << player1->Name() + " [Red] wins!\n";
        return 1;
    }
    else if ( board.player1_pieces().size() == 0 )
    {
        if ( ui != nullptr )
            ui->end_game(P_TWO, player2->Name() + " [Black] wins!");
        if ( print_console )
            cout << board;
        cout << player2->Name() << " [Black] wins!\n";
        return 2;
    }
    else if ( Rules(board, current_player).return_num_moves() == 0 )
    {
        if ( current_player == P_TWO )
        {
            if ( ui != nullptr )
                ui->end_game(P_ONE, player1->Name() + " [Black] wins! " + player2->Name() + " cannot move.");
            if ( print_console )
                cout << board;
            cout << player1->Name() << " [Black] wins! " << player2->Name() << " cannot move.\n";
            return 3;
        }
        else
        {
            if ( ui != nullptr )
                ui->end_game(P_TWO, player2->Name() + " [Red] wins! " + player1->Name() + " cannot move.");
            if ( print_console )
                cout << board;
            cout << player2->Name() << " [Red] wins! " << player1->Name() << " cannot move.\n";
            return 4;
        }
    }
    else if ( turns_since_action >= 100 ) // each player gets 50 moves
    {
        if ( ui != nullptr )
            ui->end_game(NONE,
                         "Draw: fifty moves since last capture or kinging.");
        if ( print_console )
            cout << board;
        cout << "Draw: fifty moves since last capture or kinging.\n";
        return 5;
    }
    else if ( has_repeated(board_history) )
    {
        if ( ui != nullptr )
            ui->end_game(NONE,
                         "Draw: this board state has occurred twice before.");
        if ( print_console )
            cout << board;
        cout << "\nDraw: this board state has occurred twice before.\n";
        return 6;
    }

    return false;
}

void Game::print_board(ostream& out) const
{
    out << board;
}

Board Game::return_board() const
{
    return board;
}

/****************************************************************************//*
 * @brief Runs the game's main loop.
 *
 * @returns 1 : player one won the game by capturing all of player two's pieces
 * @returns 2 : player two won the game by capturing all of player one's pieces
 * @returns 3 : player one won the game by pinning player two
 * @returns 4 : player one won the game by pinning player one
 * @returns 5 : the game was a draw by stalling (50 moves without capture/kinging)
 * @returns 6 : the game was a draw due to repeated board state
 ******************************************************************************/
int Game::run(bool noDelay)
{
    int ret_val;

    if ( print_console )
    {
        cout    <<
            "\nWelcome to checkers! This game follows the standard American\n"
                <<
            "rules for checkers. So, if you can jump your opponents you have\n"
                <<
            "to!! The first one to clear all of their opponents pieces off\n"
                <<
            "the board wins. If the board ends up in the same configuration 3\n"
                <<
            "times, the game will end in a draw. Also, if more than 50 moves \n"
                <<
            "happen without someone capturing a piece it will be a draw.\n\n";

        cout    << "Here is a guide to the game board:\n"
                << "  0 = Player 1\n"
                << "  + = Player 2\n"
                << "  @ = Player 1 King\n"
                << "  & = Player 2 King\n"
                << "  - = Available empty space\n\n";

        cout    <<
            "To enter a move you will enter the coordinates of the piece you\n"
                <<
            "want to move, then enter each subsequent space you want it to\n"
                <<
            "visit. So if you want to move player one's piece at B3 to D4, you\n"
                <<
            "would type \"B3 D4\". If you have a move that will jump multiple\n"
                << "pieces, you will need to enter each space separately. For\n"
                <<
            "example, if your piece is on B3, and you need to jump to D5 and\n"
                << "then to F7 you would type \"B3 D5 F7\".\n\n";

        cout << "Have fun and enjoy the game!!!\n\n";
    }

    do
    {
        Move move;

        if ( print_console )
            cout << board << player1->Name() << "'s turn:\n";

        update_rules();

        update_ui(P_ONE);

        if (!noDelay) usleep(1000000);

        move = player1->get_move(board, rules);

        while ( !apply_move(move) )
        {
            if (player1->Name().substr(0,5) != "Human")
            {
               badMove = PLAYER1;
               break;
            }
            if ( print_console )
                cout << "Invalid move entered, please try again: \n" << board;
            if ( ui != nullptr )
                ui->notify_invalid_move();

            move = player1->get_move(board, rules);
        }

        update_ui(P_ONE);

        ret_val = is_over();
        if ( ret_val )
            break;


        //generate the possible moves for the player
        rules = Rules(board, current_player);  //TODO: is this the best way to do this?

        update_ui(P_TWO);

        update_rules();
        if ( print_console )
            cout << board << player2->Name() << "'s turn:\n";

        if (!noDelay) usleep(1000000);

        move = player2->get_move(board, rules);

        while ( !apply_move(move) )
        {
            if (player2->Name().substr(0,5) != "Human")
            {
               badMove = PLAYER2;
               break;
            }
            if ( print_console )
                cout << "Invalid move entered, please try again: \n" << board;
            if ( ui != nullptr )
                ui->notify_invalid_move();
            move = player2->get_move(board, rules);
        }

        update_ui(P_TWO);

        ret_val = is_over();
    }
    while ( !ret_val );

    return ret_val;
}

/**
 * Update the UI (if it exists).
 */
void Game::update_ui(PlayerID pId)
{
    if ( ui != nullptr )
    {
        ui->update_turn(pId);
        ui->update_board(board);
        ui->update_death_count(12 - board.player1_pieces().size(),
                               12 - board.player2_pieces().size());
    }
}

void Game::update_rules()
{
    static PlayerID last_player = P_TWO;

    if ( current_player != last_player )
    {
        rules       = Rules(board, current_player);
        last_player = current_player;
    }
}

int cmp_kings(const PieceMap& a, const PieceMap& b)
{
    int count_a = count_if(
        a.begin(), a.end(), [](const pair<Position, Piece>& p){
            return p.second.is_king();
        });

    int count_b = count_if(
        b.begin(), b.end(), [](const pair<Position, Piece>& p){
            return p.second.is_king();
        });

    return count_a - count_b;
}

bool has_repeated(const multiset<Board>& states)
{
    if ( states.empty() )
        return false;

    auto iter   = states.begin();
    int count   = 1;
    Board cur(*iter++);

    while ( iter != states.end() && count < 3 )
    {
        if ( *iter == cur )
            ++count;
        else
        {
            cur     = *iter;
            count   = 1;
        }
        ++iter;
    }

    return count == 3;
}

bool piece_diff(Board start, Board end)
{
    if ( start.return_pieces().size() != end.return_pieces().size() )
        return true;

    if ( cmp_kings(start.player1_pieces(), end.player1_pieces()) != 0 )
        return true;

    if ( cmp_kings(start.player2_pieces(), end.player2_pieces()) != 0 )
        return true;

    return false;
}
