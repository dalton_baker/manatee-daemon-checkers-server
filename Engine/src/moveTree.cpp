/**************************************************************************//**
 * @file
 * @brief Implement the movetree class.
 *****************************************************************************/
#include <moveTree.h>


/**
 * Constructor for the class. Initialize to invalid position.
 */
MoveTree::MoveTree()
{
	this->pos = Position(-1,-1);
}

/**
 * Constructor for the class.
 *
 * @param pos - position for the node
 */
MoveTree::MoveTree(Position pos)
{
	this->pos = pos;
}


/**
 * Add the child at the given position.
 *
 * @param p - position of the new node
 */
void MoveTree::addChild(Position p)
{
	//will create if it doesn't already exist
	children.insert(pair<Position,MoveTree>(p,MoveTree(p)));
}

/**
 * Get a child of this node.
 * @param pos - position of the child node.
 * @return the child node.
 */
MoveTree& MoveTree::getChild(Position pos)
{
	//return the node if it exists
	if (hasChild(pos))
	{
		return children.at(pos);
	}

	//return a dummy
	return *(new MoveTree(Position(-1,-1)));
}

/**
 * Give the positions of this node's children.
 * @return vector of child positions.
 */
vector<Position> MoveTree::getChildPositions()
{
	vector<Position> nodePositions;

	//iterate through children and add positions to the vector
	for (auto child : children)
	{
		nodePositions.push_back(child.first);
	}

	return nodePositions;
}

/**
 * Check if a child exists.
 *
 * @param pos - position to check
 * @return result of the check.
 */
bool MoveTree::hasChild(Position pos)
{
	return children.find(pos) != children.end();
}

/**
 * Check whether the node has any children.
 * @return result of the check.
 */
bool MoveTree::hasChildren()
{
	return !children.empty();
}

/**
 * Get the position of the node.
 * @return the position.
 */
Position MoveTree::getPos()
{
	return pos;
}


/**
 * Insert a move queue into the tree.
 * @param m - move to insert.
 */
void MoveTree::insert(Move m)
{
	//recursively add the move to the tree
    if (!m.empty())
    {
        Position pos = m.front();
        m.pop();

        //create the node if it doesn't exist
        if (!hasChild(pos))
        {
        	addChild(pos);
        }

        //traverse down the tree
        children.at(pos).insert(m);
    }
}

/**
 * Get options for next location for a partial move.
 * @param m - the move.
 * @return a vector of move option positions.
 */
vector<Position> MoveTree::getMoveOptions(Move m)
{
	MoveTree* tree = this;
	vector<Position> moveOptions;

	//traverse down tree following the move queue
	while (!m.empty() && tree->hasChild(m.front()))
	{
		//get the child tree and next position
		tree = &(tree->getChild(m.front()));
		m.pop();
	}

	//only return children if entire move queue matched.
	if (m.empty())
	{
		//add the children of this node
		for (auto & t : tree->children)
		{
			moveOptions.push_back(t.first);
		}
	}

	return moveOptions;
}


/**
 * Check if a move queue is contained in the movetree.
 * @param m - move to check
 * @return result of the check.
 */
bool MoveTree::moveIsValid(Move m)
{
	MoveTree* tree = this;
	vector<Position> moveOptions;

	//traverse down tree following the move queue
	while (!m.empty() && tree->hasChild(m.front()))
	{
		moveOptions.push_back(m.front());

		//get the child tree and next position
		tree = &(tree->getChild(m.front()));
		m.pop();
	}

	//match: used entire queue and we are at a leaf in the tree
	return m.empty() && !tree->hasChildren();
}
