
#include "utilities.h"
//this allows us to print Positions easily
ostream& operator<<(ostream& out, const Position& pos)
{
    static const char COLUMNS[] = "ABCDEFGH";
    static const char ROWS   [] = "12345678";

    return out << COLUMNS[pos.first - 1] << ROWS[pos.second - 1];
}

bool operator<(const Position lhs, const Position rhs)
{
    return lhs.second < rhs.second;
}

//this allows us to print Moves easily
ostream& operator<<(ostream& out, Move move)
{
    out << '[';
    while ( !move.empty() )
    {
        out << move.front();
        move.pop();
        if ( !move.empty() )
            out << ", ";
    }
    out << ']';
    return out;
}

bool operator==(const Move lhs, const Move rhs)
{
    Move m1 = lhs;
    Move m2 = rhs;

    //get out if the moves are not the same length
    if( m1.size() != m2.size() )
    {
        return false;
    }

    //compare each element of the move set
    while( !m1.empty() || !m2.empty())
    {
        if( m1.front().first != m2.front().first ||
            m1.front().second != m2.front().second )
        {
            return false;
        }

        m1.pop();
        m2.pop();
    }

    return true;
}
