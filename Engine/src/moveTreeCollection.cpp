/**************************************************************************//**
 * @file
 * @brief Implement the movetree collection class.
 *****************************************************************************/
#include <moveTreeCollection.h>


/**
 * Constructor for the class.
 */
MoveTreeCollection::MoveTreeCollection() { }

/**
 * Add a MoveTree to the collection if a tree for the starting position doesn't
 * already exist.
 * @param tree - the tree to add.
 */
void MoveTreeCollection::addTree(MoveTree& tree)
{
	Position startPos = tree.getPos();

	//add the tree if it doesn't exist already
	treeCollection.insert(pair<Position,MoveTree>(startPos, tree));
}


/**
 * Add a MoveTree for the given starting position.
 * @param pos - the starting position for the tree
 */
void MoveTreeCollection::addTree(Position pos)
{
	//Create a new tree at the starting position and try to add it.
	addTree(*(new MoveTree(pos)));
}

/**
 * Check if there is a MoveTree for the given starting position.
 * @param pos - starting position for the MoveTree
 * @return result of the check
 */
bool MoveTreeCollection::hasTree(Position pos)
{
	return !(treeCollection.find(pos) == treeCollection.end());
}

/**
 * Get the MoveTree for the given position if it exists.
 * @param pos - starting position of the MoveTree.
 * @return MoveTree if it exists. Dummy otherwise.
 */
MoveTree& MoveTreeCollection::getTree(Position pos)
{
	//return the tree if it exists
	if (hasTree(pos))
	{
		return treeCollection.at(pos);
	}

	//return a dummy
	return *(new MoveTree(Position(-1,-1)));
}

/**
 * Add a move to the MoveTree collection. Create the MoveTree if move starts at
 * a new position.
 * @param m - move to add.
 */
void MoveTreeCollection::insert(Move m)
{
	Position startPos;

	if (!m.empty())
	{
		//get the starting position for the move
		startPos = m.front();
		m.pop();

		//create the tree if it doesn't exist
		if (!hasTree(startPos))
		{
			addTree(startPos);
		}

		//Add the move to the tree
		treeCollection.at(startPos).insert(m);
	}
}

/**
 * Return an iterator pointing to the first element in MoveTreeCollection.
 * @return an iterator to the beginning of the collection.
 */
MoveTreeCollection::iterator MoveTreeCollection::begin()
{
	return treeCollection.begin();
}

/**
 * Return an iterator pointing to the past-the-end element in MoveTreeCollection.
 * @return an iterator to the end of the collection.
 */
MoveTreeCollection::iterator MoveTreeCollection::end()
{
	return treeCollection.end();
}


/**
 * Get the positions of the moveable pieces.
 * @return a vector of piece positions.
 */
vector<Position> MoveTreeCollection::getMoveablePieces()
{
	vector<Position> optionPositions;

	for (auto & tree : treeCollection)
	{
		if (tree.second.hasChildren())
		{
			optionPositions.push_back(tree.first);
		}
	}

	return optionPositions;
}

/**
 * Get options for next location for a partial move.
 * @param m - the move.
 * @return a vector of move option positions.
 */
vector<Position> MoveTreeCollection::getMoveOptions(Move m)
{
	Position movePos;

	if (!m.empty())
	{
		movePos = m.front();
		if (hasTree(movePos))
		{
			m.pop();
			return treeCollection.at(movePos).getMoveOptions(m);
		}
	}
	else //no moves in the move queue.
	{
		return getMoveablePieces();
	}

	return vector<Position>();
}

/**
 * Check if a move queue is contained in the movetree.
 * @param m - move to check
 * @return result of the check.
 */
bool MoveTreeCollection::moveIsValid(Move m)
{
	Position movePos;

	if (!m.empty())
	{
		movePos = m.front();
		if (hasTree(movePos))
		{
			m.pop();
			return treeCollection.at(movePos).moveIsValid(m);
		}
	}

	return false;
}

