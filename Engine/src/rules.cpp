#include "rules.h"

Rules::Rules(Board curr_state, PlayerID player_id)
{
    //get a map of all of your players pieces
    if( player_id == P_ONE )
        curr_pieces = curr_state.player1_pieces();
    else
        curr_pieces = curr_state.player2_pieces();
    curr_board = curr_state;
    curr_player = player_id;

    //find all jump moves
    find_jump_moves();

    //if there aren't any jump moves, find regular moves
    if( possible_moves.empty() )
        find_reg_moves();
}

//adds any regular moves on the board to the list of possible moves
void Rules::find_reg_moves()
{
    //cycle through each piece we have
    for(auto p : curr_pieces)
    {
        //cycle through each diagonal location for each piece
        for( Position diag : get_diag_loc(p.first, 1) )
        {
            //check if the piece is moving in the correct dirrection
            //also make sure the new locatiuon is a valid place
            if( curr_board.is_empty( diag ) &&
                ( ( p.first < diag && curr_player == P_ONE ) ||
                  ( diag < p.first && curr_player == P_TWO ) ||
                  ( p.second.is_king() ) ) )
            {
                Move new_move;
                Board new_board(curr_board);

                //create new move
                new_move.push(p.first);
                new_move.push(diag);

                //create the new board state
                new_board.move_piece( p.first, diag );

                //check for king
                find_king(diag, new_board);

                //create the move board pair and add it to the vector
                possible_moves.push_back(make_pair(new_move, new_board));
            }
        }
    }
}

void Rules::find_jump_moves()
{
    //cycle through each piece we have
    for(auto p : curr_pieces)
    {
        Move new_move;
        new_move.push(p.first);
        Board new_board(curr_board);

        //see if a jump move exists
        find_next_jump(p.first, p.second.is_king(), new_move, new_board);
    }
}

void Rules::find_next_jump(Position p, bool is_king, Move m, Board b)
{
    //we need to keep track of the number of possible moves, so we don't
    //add shorter moves that are part of longer moves
    unsigned int num_pos_moves = possible_moves.size();

    //cycle through each diagonal location for each piece
    for( Position diag : get_diag_loc(p, 2) )
    {
        Move pass_move = m;
        Board pass_board = b;
        //check if the piece is moving in the correct dirrection
        //also make sure the new locatiuon is a valid place
        if( b.is_empty( diag ) &&
            ( ( p < diag && curr_player == P_ONE ) ||
              ( diag < p && curr_player == P_TWO ) ||
              ( is_king ) ) )
        {
            //Find the position being jumped
            Position jumped_piece((diag.first + p.first)/2,
                              (diag.second + p.second)/2 );

            //get a map of the opponents pieces
            PieceMap opp_pieces;
            if( curr_player == P_ONE )
                opp_pieces = b.player2_pieces();
            else
                opp_pieces = b.player1_pieces();

            //check if the jumped piece is in the opponents piece map
            if( opp_pieces.find(jumped_piece) != opp_pieces.end() )

            {
                //create the new board state
                pass_board.move_piece( p, diag );
                pass_board.kill_piece( jumped_piece );

                //create new move
                pass_move.push(diag);

                //call again to add more possible jumps
                find_next_jump( diag, is_king, pass_move, pass_board);
            }
        }
    }

    //check that there was a move added, also make sure no longer moves
    //that contain this move were added
    if(m.size() > 1 && num_pos_moves == possible_moves.size())
    {
        //check for king
        find_king(m.back(), b);
        possible_moves.push_back(make_pair(m, b));
    }
}

void Rules::find_king(Position p, Board &b)
{
    //check if the p1 piece made it to the opposit side of the board
    if( curr_player == P_ONE && p.second == 8 )
    {
        b.king_piece(p);
    }
    //check if the p2 piece made it to the opposit side of the board
    else if( curr_player == P_TWO && p.second == 1 )
    {
        b.king_piece(p);
    }
}

//return a set of all the diagonal moves from a location
vector<Position> Rules::get_diag_loc(Position p, int dist)
{
    vector<Position> diag_loc;

    //check the validity of each possible diagonal location and add them
    if( p.first-dist > 0 && p.second-dist > 0 )
        diag_loc.push_back( make_pair(p.first-dist, p.second-dist) );
    if( p.first-dist > 0 && p.second+dist < 9 )
        diag_loc.push_back( make_pair(p.first-dist, p.second+dist) );
    if( p.first+dist < 9 && p.second-dist > 0 )
        diag_loc.push_back( make_pair(p.first+dist, p.second-dist) );
    if( p.first+dist < 9 && p.second+dist < 9 )
        diag_loc.push_back( make_pair(p.first+dist, p.second+dist) );

    return diag_loc;
}




//This is the begining of functions you need to care about!!!!
/**************************************************************************//**
 * Check to see if a move is in the list of possible moves
 * @param move - the move you are checking for
 * @returns true if the move is valid, false if not
 *****************************************************************************/
bool Rules::find_move( Move move ) const
{
    //cycle through each move, checking for user input move
    for( auto moveAndBoard : possible_moves )
    {
        //if a move matches return true
        if( move == moveAndBoard.first )
        {
            return true;
        }
    }

    return false;
}

/**************************************************************************//**
 * Returns the board state that will exist after a specific move.
 * This will return a board in the starting game possition if the move is not
 * valid!!! This function assumes you have a move that is valid!!
 * @param move - the move you are checking for
 * @returns the board state that occurs after the provided move
 *****************************************************************************/
Board Rules::get_board( Move move ) const
{
    //cycle through each move, checking for user input move
    for( auto moveAndBoard : possible_moves )
    {
        //if a move matches, change the board and return true
        if( move == moveAndBoard.first )
        {
            return moveAndBoard.second;
        }
    }

    //returns a board in the starting state if move doesn't exist
    Board return_board;
    return return_board;
}

/**************************************************************************//**
 * Get the number of possible moves for the current player
 * @returns The number of moves
 *****************************************************************************/
int Rules::return_num_moves() const
{
    return possible_moves.size();
}

/**************************************************************************//**
 * Returns all the possible moves in the form of a moveTreeCollection
 * @returns The collection of moves
 *****************************************************************************/
MoveTreeCollection Rules::return_move_tree() const
{
	MoveTreeCollection moves;

    //iterate through the set of possible moves
    for( auto moveAndBoard : possible_moves )
    {
    	//Add the move to the tree
    	moves.insert(moveAndBoard.first);
    }

    return moves;
}

/**************************************************************************//**
 * Returns all the possible moves in the form of a vector of moves
 * @returns The collection of moves
 *****************************************************************************/
vector<Move> Rules::return_move_vect() const
{
    vector<Move> move_return;

    for( auto moveAndBoard : possible_moves )
    {
        move_return.push_back(moveAndBoard.first);
    }

    return move_return;
}

/**************************************************************************//**
 * This will return a rules object. It will contain the rules that will exist
 * for your opponent when your turn is finished if you make the provided move.
 * This function assumes you have a move that is valid!! If you give it a move
 * that doesn't exist you will get the rules for the first turn of a game.
 * @param move - the move you are checking for
 * @returns the next rules object that will exist if you make the move provided
 *****************************************************************************/
Rules Rules::get_next_rules( Move m ) const
{
    if(curr_player == P_ONE)
        return Rules(get_board(m) , P_TWO);

    return Rules(get_board(m), P_ONE);
}
