/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the Board class
 *****************************************************************************/
#include "board.h"

Board::Board()
{
    //place all of player 1's pieces
    the_board[make_pair(2, 1)]  = Piece(P_ONE);
    the_board[make_pair(4, 1)]  = Piece(P_ONE);
    the_board[make_pair(6, 1)]  = Piece(P_ONE);
    the_board[make_pair(8, 1)]  = Piece(P_ONE);
    the_board[make_pair(1, 2)]  = Piece(P_ONE);
    the_board[make_pair(3, 2)]  = Piece(P_ONE);
    the_board[make_pair(5, 2)]  = Piece(P_ONE);
    the_board[make_pair(7, 2)]  = Piece(P_ONE);
    the_board[make_pair(2, 3)]  = Piece(P_ONE);
    the_board[make_pair(4, 3)]  = Piece(P_ONE);
    the_board[make_pair(6, 3)]  = Piece(P_ONE);
    the_board[make_pair(8, 3)]  = Piece(P_ONE);

    //place all of player 2's pieces
    the_board[make_pair(1, 6)]  = Piece(P_TWO);
    the_board[make_pair(3, 6)]  = Piece(P_TWO);
    the_board[make_pair(5, 6)]  = Piece(P_TWO);
    the_board[make_pair(7, 6)]  = Piece(P_TWO);
    the_board[make_pair(2, 7)]  = Piece(P_TWO);
    the_board[make_pair(4, 7)]  = Piece(P_TWO);
    the_board[make_pair(6, 7)]  = Piece(P_TWO);
    the_board[make_pair(8, 7)]  = Piece(P_TWO);
    the_board[make_pair(1, 8)]  = Piece(P_TWO);
    the_board[make_pair(3, 8)]  = Piece(P_TWO);
    the_board[make_pair(5, 8)]  = Piece(P_TWO);
    the_board[make_pair(7, 8)]  = Piece(P_TWO);
}

Board::Board(const Board& b)
{
    the_board = b.the_board;
}

//moves a piece to a new location
bool Board::move_piece(Position start, Position end)
{
    if ( the_board.find(start) != the_board.end() )
    {
        the_board[end] = the_board.at(start);
        the_board.erase(start);
        return true;
    }
    return false;
}

//removes a piece from the board
bool Board::kill_piece(Position p)
{
    if ( the_board.find(p) != the_board.end())
    {
        the_board.erase(p);
        return true;
    }
    return false;
}

//Kings a player piece
bool Board::king_piece(Position p)
{
    if ( the_board.find(p) != the_board.end())
    {
        the_board.at(p).king_piece();
        return true;
    }
    return false;
}

const PieceMap Board::player1_pieces() const
{
    PieceMap locations;

    for ( auto x : the_board )
        if ( x.second.is_player1() )
            locations[x.first] = x.second;

    return locations;
}

const PieceMap Board::player2_pieces() const
{
    PieceMap locations;

    for ( auto x : the_board )
        if ( x.second.is_player2() )
            locations[x.first] = x.second;

    return locations;
}

const PieceMap Board::return_pieces() const
{
    return the_board;
}

bool Board::is_player1(Position p) const
{
    if( !is_empty(p) )
        return the_board.at(p).is_player1();
    return false;
}

bool Board::is_player2(Position p) const
{
    if( !is_empty(p) )
        return the_board.at(p).is_player2();
    return false;
}

bool Board::is_king(Position p) const
{
    if( !is_empty(p) )
        return the_board.at(p).is_king();
    return false;
}

bool Board::is_empty(Position p) const
{
    return the_board.find(p) == the_board.end();
}

// check if two boards are identical
bool Board::operator==(const Board& other) const
{
    return this->the_board == other.the_board;
}

// defines a weak ordering over boards
// used to sort boards for repeated board checks
bool Board::operator<(const Board& other) const
{
    const PieceMap& lh  = this->the_board;
    const PieceMap& rh  = other.the_board;

    if ( lh.size() != rh.size() )
        return lh.size() < rh.size();

    for ( int i = 0, j; i < 8; ++i )
    {
        for ( j = 0; j < 8; ++j )
        {
            PieceMap::const_iterator lit    = lh.find(Position(i, j));
            PieceMap::const_iterator rit    = rh.find(Position(i, j));

            Piece lpiece =
                ( lit != lh.end() ? ( *lit ).second : Piece(NONE) );
            Piece rpiece =
                ( rit != rh.end() ? ( *rit ).second : Piece(NONE) );

            if ( !( lpiece == rpiece ) )
                return lpiece < rpiece;
        }
    }

    return false;
}

//this will let us print the board easily
ostream& operator<<(ostream& out, const Board& board)
{
    out << "  A B C D E F G H\n";
    for ( int j, i = 0; i < 8; i++ )
    {
        out << ( i + 1 );
        for ( j = 0; j < 8; j++ )
        {
            out << " ";
            //a valid space alwayse has a odd and an even number
            if ( ( i % 2 == 1 && j % 2 == 0 ) || ( i % 2 == 0 && j % 2 == 1 ) )
            {
                if ( board.the_board.find(make_pair(j + 1, i + 1)) !=
                     board.the_board.end() )
                    out << board.the_board.at(make_pair(j + 1, i + 1));
                else
                    out << "-";
            }
            else
                out << " ";
        }
        out << ' ' << ( i + 1 ) << '\n';
    }
    out << "  A B C D E F G H\n";

    return out;
}
