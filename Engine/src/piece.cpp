/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the Piece class
 *****************************************************************************/
#include "piece.h"

Piece::Piece(PlayerID p_id)
{
    player_id   = p_id;
    king        = false;
}

bool Piece::is_player1() const
{
    return player_id == P_ONE;
}

bool Piece::is_player2() const
{
    return player_id == P_TWO;
}

bool Piece::is_king() const
{
    return king;
}

void Piece::king_piece()
{
    king = true;
}

//compare Piece objects
bool Piece::operator==(const Piece& other) const
{
    return this->player_id == other.player_id && this->king == other.king;
}

bool Piece::operator<(const Piece& other) const
{
    if ( this->player_id < other.player_id )
        return true;

    if ( !this->king && other.king )
        return true;

    return false;
}

//print a piece object
ostream& operator<<(ostream& out, const Piece& p)
{
    if ( p.player_id == P_ONE && !p.king )
        out << "0";

    else if ( p.player_id == P_ONE && p.king )
        out << "@";

    else if ( p.player_id == P_TWO && !p.king )
        out << "+";

    else if ( p.player_id == P_TWO && p.king )
        out << "&";

    else
        out << " ";

    return out;
}
