#ifndef RANDOM_PLAYER_H
#define RANDOM_PLAYER_H

#include "player.h"
//This is only used for usleep
#include <unistd.h>
//These are only here for usleep and rand, feel free to get rid of them
#include <ctime>
#include <cstdlib>

class RandomPlayer : public Player
{
public:
    RandomPlayer();
    Move get_move(Board, Rules);
};

extern "C" Player* createPlayer()
{
   return new RandomPlayer();
}

extern "C" void destroyPlayer(Player *p)
{
   delete p;
}
#endif
