#include "greedyPlayer.h"

GreedyPlayer::GreedyPlayer()
{
    //generate a random seed
    //you can remove this if you arent using random numbers
    srand((unsigned)time(NULL));
    name = "GreedyPlayer";
}

Move GreedyPlayer::get_move(Board b, Rules r)
{
    //This is only here to wait for a second, 1000000 = 1 second
    //you can increse, decrease, or remove it

    //remember that GreedyPlayer is ALWAYS player 2

    int num_of_my_pieces = b.player2_pieces().size();
    vector<Move> good_moves;

    //cycle through all available moves and build a vector of good ones
    for( auto x : r.return_move_vect() )
    {
        //get the next game state for each move
        Rules example_rules = r.get_next_rules(x);
        //make sure your opponent will have moves
        if( !example_rules.return_move_vect().empty() )
        {
            //get one of the moves that will be available to your opponent
            Move example_move = example_rules.return_move_vect()[0];
            //get the board state at the end of your opponents move
            Board example_board = example_rules.get_board(example_move);
            //get the number of your pieces after your opponents move
            int pieces_after_opponent_move = example_board.player2_pieces().size();
            //check to see if the move you make will not get a piece killed
            if(pieces_after_opponent_move == num_of_my_pieces)
            {
                //if your pieces are safe for the move "x" store it as a good move
                good_moves.push_back(x);
            }
        }
    }

    //if there are no good moves, just pick a random available move
    if(good_moves.empty())
    {
        //get a random move and return it
        int move_index = rand() % r.return_num_moves();
        return r.return_move_vect()[move_index];
    }

    //if there are good moves, pick a random one
    int move_index_good = rand() % good_moves.size();
    return good_moves[move_index_good];

}
