#ifndef __HUMAN__PLAYER__H__
#define __HUMAN__PLAYER__H__

#include "player.h"
#include "ui.h"

class HumanPlayer : public Player
{
private:
    UI* ui;
public:
    HumanPlayer();
    ~HumanPlayer();

    Move get_move(Board, Rules);
    void setUI(UI *);
    void setName(const string &);
};

extern "C" Player* createPlayer()
{
   return new HumanPlayer();
}

extern "C" void destroyPlayer(Player *p)
{
   delete p;
}
#endif
