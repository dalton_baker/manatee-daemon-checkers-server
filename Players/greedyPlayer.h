#ifndef GREEDYPLAYER_H
#define GREEDYPLAYER_H

#include "player.h"
//This is only used for usleep
#include <unistd.h>
//These are only here for usleep and rand, feel free to get rid of them
#include <ctime>
#include <cstdlib>

class GreedyPlayer : public Player
{
public:
    GreedyPlayer();
    Move get_move(Board, Rules);
};

extern "C" Player* createPlayer()
{
   return new GreedyPlayer();
}

extern "C" void destroyPlayer(Player *p)
{
   delete p;
}
#endif
