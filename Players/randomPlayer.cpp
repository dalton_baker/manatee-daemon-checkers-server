#include "randomPlayer.h"

RandomPlayer::RandomPlayer()
{
    //generate a random seed
    //you can remove this if you arent using random numbers
    srand((unsigned)time(NULL));
    name = "RandomPlayer";
}

Move RandomPlayer::get_move(Board b, Rules r)
{
    //This is only here to wait for a second, 1000000 = 1 second
    //you can increse, decrease, or remove it

    //get a random move and return it

    int move_index = rand() % r.return_num_moves();
    return r.return_move_vect()[move_index];
}
