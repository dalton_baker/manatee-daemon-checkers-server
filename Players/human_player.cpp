/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the Player class
 *****************************************************************************/

#include <vector>
#include "human_player.h"

/**************************************************************************//**
 * @author Ben Catalano
 *
 * @par Description:
 * The default constructor for the Player class.
 *
 *****************************************************************************/
HumanPlayer::HumanPlayer()
{
    name = "HumanPlayer";
}

/**************************************************************************//**
 * @author
 *
 * @par Description:
 * The default destructor for the Player class.
 *
 *****************************************************************************/
HumanPlayer::~HumanPlayer() {}

void HumanPlayer::setName(const string &n)
{
   name = n;
}

void HumanPlayer::setUI(UI *uiobject)
{
   ui = uiobject;
}

/**************************************************************************//**
 * Get the move from the user through the UI.
 * @param b - the current board
 * @param validMoves - a Collection of valid MoveTrees.
 * @returns move : the move given by user if valid, else empty
 *****************************************************************************/
Move HumanPlayer::get_move(Board b, Rules r)
{
    if( ui != nullptr )
        return ui->get_move(b, r.return_move_tree());

    vector<char> str;
    Move move;
    char column, row;

    // reserve enough space for most moves (<three steps)
    str.reserve(11);

    // dynamically get cstring from input
    for ( char c; cin.get(c) && c != '\n'; str.push_back(c) );
    str.push_back('\0');

    char* tok = strtok(str.data(), ", ");

    // tokenize input into moves
    while ( tok != nullptr )
    {
        column  = tok[0];
        row     = tok[1];

        // return an empty move if the input is malformed

        if (row == '\0') // token only had one character
        {
            cout << "Error: column given without row.\n";
            return Move();
        }

        else if (column < 'A' || column > 'H')
        {
            std::cout << "Error: column is invalid.\n";
            return Move();
        }

        else if (row < '1' || row > '8')
        {
            std::cout << "Error: row is invalid.\n";
            return Move();
        }

        // convert from ASCII encoded characters to integers
        move.emplace(column - 64, row - 48);

        tok = strtok(nullptr, ", ");
    }

    return move;
}
