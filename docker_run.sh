xhost +local:root

docker run -ti \
	-v "`pwd`":/home/dev/platform \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	-e DISPLAY=$DISPLAY \
    -e LD_LIBRARY_PATH=lib \
    -e CHECKER_PLAYERDIR=Players \
	checkers sh -c 'cd /home/dev/platform && bash'
