In order for the makefiles to work properly:
Header files need to go in the "inc" folder.
Source files need to go in the "src" folder.
Unit tests need to go in the "test/src" folder.
catch.hpp needs to be in the "test/inc" folder.

This is the case for each of the (currently three) individual units.
