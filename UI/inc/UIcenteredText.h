/**************************************************************************//**
 * @file
 * @brief This is the header for the UIboard.cpp file
 *****************************************************************************/
#ifndef __UICENTEREDTEXT__H__
#define __UICENTEREDTEXT__H__


#include "UIelement.h"

/*!
 * @brief UIboard documantation
 */
class UIcenteredText : public UIelement
{
protected:
	int size = 12;
	string str = "";
	wchar_t* strUnicode = nullptr;
	sf::Text::Style style = sf::Text::Bold;
	sf::Font unicodeFont;
	sf::Font normalFont;

	sf::Text buildElement();

public:
	UIcenteredText( );
	UIcenteredText(int, int);
    ~UIcenteredText();

    void setCharacterSize(int);
    void setString(string);
    void setString(wchar_t[]);
    void setCharacterStyle(sf::Text::Style);
    void setPositionCentered(float, float);
    void setPosition(float, float);
    sf::FloatRect getBounds();
    void draw(sf::RenderWindow&);
    bool containsPosition(Position);

};

#endif
