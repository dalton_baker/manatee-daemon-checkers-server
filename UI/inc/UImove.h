/**************************************************************************//**
 * @file
 * @brief This is the header for the UImove.cpp file
 *****************************************************************************/
#ifndef __UIMOVE__H__
#define __UIMOVE__H__

#include "UIboard.h"
#include <math.h>

/*!
 * @brief UImove documantation
 */
class UImove : public UIelement
{
protected:
   Move curr_move;
   Position centerPos;
   double radius;

public:
   UImove(Position, int);
   ~UImove();

   void draw(sf::RenderWindow&);
   bool containsPosition(Position);

};

#endif
