/**************************************************************************//**
 * @file
 * @brief This is the header for the UIboard.cpp file
 *****************************************************************************/
#ifndef __UIBOARD__H__
#define __UIBOARD__H__


#include "UIelement.h"

/*!
 * @brief UIboard documantation
 */
class UIboard : public UIelement
{
protected:
    /*! This is an example variable and how to document a class variable */
    int grid_size;
    pair<int, int> grid_pos[8][8];
public:
    UIboard(int, int, int);
    ~UIboard();

    sf::FloatRect getBounds();
    Position gridToWinPos(Position pos);
    void draw(sf::RenderWindow&);
    bool containsPosition(Position);

};

#endif
