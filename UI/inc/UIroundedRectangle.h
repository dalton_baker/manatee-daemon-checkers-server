/**************************************************************************//**
 * @file
 * @brief This is the header for the UIroundedRectangle.cpp file
 *****************************************************************************/
#ifndef __UIROUNDEDRECTANGLE__H__
#define __UIROUNDEDRECTANGLE__H__


#include "UIelement.h"
#include "math.h"

/*!
 * @brief UIboard documantation
 */
class UIroundedRectangle : public UIelement
{
protected:
	int radius = 0;
	sf::Color color;
	sf::Color borderColor;
	int borderThickness = 0;
    void addRadiusPoints(sf::ConvexShape&, float, float, int);
public:
	UIroundedRectangle();
	UIroundedRectangle(int, int);
    ~UIroundedRectangle();

    void setRadius(int);
    void setColor(sf::Color);
    void setBorderColor(sf::Color);
    void setBorderThickness(int);
    void setPosition(float, float);
    void setPositionCentered(float, float);
    void draw(sf::RenderWindow&);
    bool containsPosition(Position);

};

#endif
