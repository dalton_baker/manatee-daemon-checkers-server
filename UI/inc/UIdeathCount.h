/**************************************************************************//**
 * @file
 * @brief This is the header for the UIdeathCount.cpp file
 *****************************************************************************/
#ifndef __UIDEATHCOUNT__H__
#define __UIDEATHCOUNT__H__


#include "UIelement.h"
#include "UIcenteredText.h"
#include "UIroundedRectangle.h"
#include "UIpiece.h"

/*!
 * @brief UIboard documantation
 */
class UIdeathCount : public UIelement
{
protected:
	PlayerID currentTurn = NONE;
	int p1Deaths = 0;
	int p2Deaths = 0;
public:
	UIdeathCount(int);
    ~UIdeathCount();

    sf::FloatRect getBounds();
    void setCurrentTurn(PlayerID);
    void setDeathCounts(int, int);
    void setPosition(float, float);
    void draw(sf::RenderWindow&);
    bool containsPosition(Position);
};

#endif
