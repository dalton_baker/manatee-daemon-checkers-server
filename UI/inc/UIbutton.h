/**************************************************************************//**
 * @file
 * @brief This is the header for the UIbuttons.cpp file
 *****************************************************************************/
#ifndef __UIBUTTON__H__
#define __UIBUTTON__H__

#include "UIelement.h"
#include "UIcenteredText.h"

/*!
 * @brief UIbuttons documantation
 */
class UIbutton : public UIelement
{
protected:
	string str;
	wchar_t * strUnicode = nullptr;
	sf::Color color;
	sf::Color borderColor;
	bool enabled = true;
	int charSize = 24;

public:
    UIbutton(int, int);
    UIbutton();
    ~UIbutton();

    void setString(string);
    void setString(const wchar_t*);
    void setColor(sf::Color);
    void setBorderColor(sf::Color);
    void setCharacterSize(int);
    void setPosition(float, float);
    void setPositionCentered(float, float);
    void setEnabled();
    void setDisabled();
    void draw(sf::RenderWindow&);
    bool containsPosition(Position);
    void handleClick(Position pos);

};

#endif
