/**************************************************************************//**
 * @file
 * @brief This is the header for the UIpieces.cpp file
 *****************************************************************************/
#ifndef __UIPIECE__H__
#define __UIPIECE__H__

#include "UIelement.h"
#include <math.h>

/*!
 * @brief UIpieces documantation
 */
class UIpiece : public UIelement
{

public:
    enum State { NORMAL, SELECTABLE, SELECTED };

    UIpiece();
    UIpiece(Piece, Position, double);
    ~UIpiece();

    void draw(sf::RenderWindow&);
    bool containsPosition(Position);
    void setState(State);
    void setPosition(Position pos);
    void setKingIcon(sf::Texture& kingIcon);


protected:
    Piece piece;
    Position centerPos;
    double radius;
    State state = NORMAL;
    sf::Texture* kingIcon = nullptr;

    void loadImage();
};

#endif
