/**************************************************************************//**
 * @file
 * @brief This is the header for the UIboard.cpp file
 *****************************************************************************/
#ifndef __UIGAMEEND__H__
#define __UIGAMEEND__H__


#include "UIelement.h"
#include "UIcenteredText.h"
#include "UIroundedRectangle.h"
#include "UIpiece.h"
#include "UIbutton.h"

/*!
 * @brief UIboard documantation
 */
class UIgameEnd : public UIelement
{
protected:
	PlayerID winner = NONE;
	string message = "";
	UIbutton button;
public:
    UIgameEnd(int, int, int);
    ~UIgameEnd();

    sf::FloatRect getBounds();
    void setWinner(PlayerID);
    void setMessage(string);
    void draw(sf::RenderWindow&);
    bool containsPosition(Position);
    void handleClick(Position pos);
};

#endif
