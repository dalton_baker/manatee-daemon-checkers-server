/**************************************************************************//**
 * @file
 * @brief This is the header for the UIelement.cpp file
 *****************************************************************************/
#ifndef __UIELEMENT__H__
#define __UIELEMENT__H__


#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "board.h"
#include <functional>

#define NO_SELECTION make_pair(-1, -1);
#define ACCEPT_MOVE make_pair(-2, -2);
#define CLEAR_MOVE make_pair(-3, -3);

/*!
 * @brief UIelement documantation
 */
class UIelement
{
protected:
	sf::FloatRect bounds;
	function<void(Position)> clickCallback;

public:
    virtual ~UIelement() {}
    virtual void draw(sf::RenderWindow&) = 0;
    virtual bool containsPosition(Position) = 0;
    virtual void handleClick(Position pos);
    virtual void onClick(function<void()> callback);
    virtual void onClick(function<void(Position)> callback);
};

#endif
