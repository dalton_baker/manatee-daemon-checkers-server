/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIpieces class
 *****************************************************************************/

#include <UIpiece.h>

/**
 * Constructor for the class.
 * @param piece - piece object for this UI element.
 * @param pos - screen position for the center of the element.
 * @param radius - radius of the piece.
 * @param state - selection state. NORMAL (default), SELECTABLE, SELECTED.
 */
UIpiece::UIpiece(Piece piece, Position pos, double radius)
	: piece(piece), centerPos(pos), radius(radius)
{
}

/**
 * Constructor for the class. Fill class with dummy values.
 */
UIpiece::UIpiece()
{
	piece = Piece(NONE);
	centerPos = Position(0, 0);
	radius = 0;
}

/**
 * Get the king icon
 * @param kingIcon - king icon texture.
 */
void UIpiece::setKingIcon(sf::Texture& kingIcon)
{
	this->kingIcon = &kingIcon;
}

/**************************************************************************//**
 * @author
 *
 * @par Description:
 * The default destructor for the UIpieces class.
 *
 *****************************************************************************/
UIpiece::~UIpiece()
{
}

/**
 * Create the piece element and draw it to the window.
 * @param window - window for drawing.
 */
void UIpiece::draw(sf::RenderWindow& window)
{
	sf::CircleShape newPiece(radius);

    //outline
	if (state == SELECTABLE)
	{
		newPiece.setOutlineThickness(5);
		newPiece.setOutlineColor(sf::Color(240,230,140));  //yellowish
	}
	else if (state == SELECTED)
	{
		newPiece.setOutlineThickness(5);
		newPiece.setOutlineColor(sf::Color(240,240,240));
	}
	else
	{
		newPiece.setOutlineThickness(2);
		newPiece.setOutlineColor(sf::Color(150, 150, 150));
	}

	//color
	if(piece.is_player2())
		newPiece.setFillColor(sf::Color::Red);
	else
		newPiece.setFillColor(sf::Color::Black);

	//position
	newPiece.setOrigin(radius, radius);
	newPiece.setPosition(centerPos.first, centerPos.second);

    window.draw(newPiece);

    //king icon
	if(piece.is_king())
	{
		if (kingIcon != nullptr)
		{
			sf::Sprite sprite;
			sprite.setTexture(*kingIcon);
			sf::FloatRect iconBounds = sprite.getGlobalBounds();
			sprite.setOrigin(iconBounds.left + iconBounds.width/2.0f,
					iconBounds.top  + iconBounds.height/2.0f);
			sprite.setPosition(centerPos.first, centerPos.second);
			sprite.setScale(40.0 / iconBounds.width, 40.0 / iconBounds.width);

			window.draw(sprite);
		}
		else
		{
			sf::Font font;
			font.loadFromFile("./UI/src/arial.ttf");
			sf::Text kingIcon;

			//font
			kingIcon.setFont(font);
			kingIcon.setString("K");
			kingIcon.setCharacterSize(24);
			//kingIcon.setFillColor(sf::Color::White);
			kingIcon.setStyle(sf::Text::Bold);

			//position
			sf::FloatRect textRect = kingIcon.getLocalBounds();
			kingIcon.setOrigin(textRect.width/2.0f, textRect.height/2.0f);
			kingIcon.setPosition(centerPos.first, centerPos.second);

			window.draw(kingIcon);
		}
	}
}


/**
 * Check if the UI element contains the given position in the window.
 * @param pos - Position in the window
 * @return true if the point is inside the element.
 */
bool UIpiece::containsPosition(Position pos)
{
	float diffX = pos.first - centerPos.first;
	float diffY = pos.second - centerPos.second;

	//dist between ui element center and given position
	float posDist = sqrt(pow(diffX, 2) + pow(diffY, 2));

	return posDist <= radius;
}

/**
 * Set the state for the piece. This can be NORMAL, SELECTABLE, SELECTED.
 * @param state - State option.
 */
void UIpiece::setState(State state)
{
	this->state = state;
}

/**
 * Set the position for the piece.
 * @param pos - Position.
 */
void UIpiece::setPosition(Position pos)
{
	centerPos = pos;
}
