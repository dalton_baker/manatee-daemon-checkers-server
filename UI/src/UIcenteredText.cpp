/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIcenteredText class
 *****************************************************************************/

#include "UIcenteredText.h"

/**
 * Constructor for the class.
 * @param width - height of the element.
 * @param height - height of the element.
 */
UIcenteredText::UIcenteredText(int width, int height)
{
	//use a placeholder value for height.
    bounds = sf::FloatRect(0, 0, width, height);

    normalFont.loadFromFile("./UI/src/arial.ttf");
    unicodeFont.loadFromFile("./UI/src/seguisym.ttf");
}

/**
 * Create an instance with dummy values.
 */
UIcenteredText::UIcenteredText()
{
	//use placeholder values
    bounds = sf::FloatRect(0, 0, 0, 0);

    normalFont.loadFromFile("./UI/src/arial.ttf");
	unicodeFont.loadFromFile("./UI/src/seguisym.ttf");
}

/**
 * Set the position for the element.
 * @param left - horizontal position.
 * @param top - vertical position.
 */
void UIcenteredText::setPosition(float left, float top)
{
	bounds.left = left;
	bounds.top = top;
}

/**
 * Set the position for the element using centered positioning. The middle of
 * the element will be at the given position.
 * @param x - horizontal position.
 * @param y - vertical position.
 */
void UIcenteredText::setPositionCentered(float x, float y)
{
	bounds.left = x - bounds.width / 2;
	bounds.top = y - bounds.height / 2;
}

/**
 * Get the bounds of the element.
 * @return the rectangle bounds object.
 */
sf::FloatRect UIcenteredText::getBounds()
{
	//Build the element to get updated bounds
	sf::Text txtElement = buildElement();

	return txtElement.getGlobalBounds();
}

/**
 * Set the message for the text area.
 * @param str - message to display with the result.
 */
void UIcenteredText::setString(string str)
{
	this->str = str;
}

/**
 * Set a unicode message for the text area.
 * @param str - unicode text for the text area.
 */
void UIcenteredText::setString(wchar_t str[])
{
	strUnicode = str;
}

/**
 * Set the size of the font.
 * @param size - size for the text.
 */
void UIcenteredText::setCharacterSize(int size)
{
	this->size = size;
}


/**
 * Set the style of the font.
 * @param size - size for the text.
 */
void UIcenteredText::setCharacterStyle(sf::Text::Style style)
{
	this->style = style;
}


UIcenteredText::~UIcenteredText()
{

}

/**
 * Creat the board and draw it to the screen.
 * @param window
 */
void UIcenteredText::draw(sf::RenderWindow& window)
{
	//create the element
	sf::Text txtElement = buildElement();

	//update the bounds
	bounds = txtElement.getGlobalBounds();

	//draw
	window.draw(txtElement);
}

/**
 * Build the text element
 * @return reference to the constructed text element
 */
sf::Text UIcenteredText::buildElement()
{
	//center of the element
	float posX = bounds.left + bounds.width/2.0f;
	float posY = bounds.top + bounds.height/2.0f;

	//create the element
	sf::Text txtElement;
	if (strUnicode != nullptr)
	{
		txtElement.setString(strUnicode);
		txtElement.setFont(unicodeFont);
	}
	else
	{
		txtElement.setString(str);
		txtElement.setFont(normalFont);
	}
	txtElement.setCharacterSize(size);
	txtElement.setStyle(style);

	//center within the bounds
	sf::FloatRect textRect = txtElement.getLocalBounds();
	txtElement.setOrigin(textRect.left + textRect.width/2.0f,
				   textRect.top  + textRect.height/2.0f);
	txtElement.setPosition(sf::Vector2f(posX,posY));

	//update the bounds
	bounds = txtElement.getGlobalBounds();

	return txtElement;
}

/**
 * Check whether the given position is within the element.
 * @param pos - the position to check.
 * @return the result of the check.
 */
bool UIcenteredText::containsPosition(Position pos)
{
	return bounds.contains(pos.first, pos.second);
}

