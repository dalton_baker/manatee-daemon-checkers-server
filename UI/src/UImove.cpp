/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UImove class
 *****************************************************************************/

#include "UImove.h"

/**
 * Constructor for the class.
 * @param pos - center position for the element
 * @param radius - radius of the element
 */
UImove::UImove(Position pos, int radius) : centerPos(pos), radius(radius)
{
}


/**************************************************************************//**
 * @author
 *
 * @par Description:
 * The default destructor for the UImove class.
 *
 *****************************************************************************/
UImove::~UImove()
{
}


/**
 * Create the move element and draw it to the window.
 * @param window - window for drawing.
 */
void UImove::draw(sf::RenderWindow& window)
{
    sf::CircleShape newMove(radius);

	newMove.setFillColor(sf::Color::Green);

	//position
	newMove.setOrigin(radius, radius);
	newMove.setPosition(centerPos.first, centerPos.second);

    window.draw(newMove);
}

/**
 * Check if the UI element contains the given position in the window.
 * @param pos - Position in the window
 * @return true if the point is inside the element.
 */
bool UImove::containsPosition(Position pos)
{
	float diffX = pos.first - centerPos.first;
	float diffY = pos.second - centerPos.second;

	//dist between ui element center and given position
	float posDist = sqrt(pow(diffX, 2) + pow(diffY, 2));

	return posDist <= radius;
}
