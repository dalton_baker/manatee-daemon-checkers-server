/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIdeathCount class
 *****************************************************************************/

#include "UIdeathCount.h"

/**
 * Constructor for the class.
 * @param width - Width of the element.
 * @param height - Height of the element.
 */
UIdeathCount::UIdeathCount(int width)
{
	//use a placeholder value for width.
    bounds = sf::FloatRect(0, 0, width, 0);
}

/**
 * Set the ID of the player who's turn it currently is.
 * @param currentTurn - ID of current player's turn.
 */
void UIdeathCount::setCurrentTurn(PlayerID currentTurn)
{
	this->currentTurn = currentTurn;
}

/**
 * Set the number of piece deaths for each player.
 * @param p1Deaths - player 1 deaths
 * @param p2Deaths - player 2 deaths
 */
void UIdeathCount::setDeathCounts(int p1Deaths, int p2Deaths)
{
	this->p1Deaths = p1Deaths;
	this->p2Deaths = p2Deaths;
}

/**
 * Set the position for the element.
 * @param x - horizontal position.
 * @param y - vertical position.
 */
void UIdeathCount::setPosition(float x, float y)
{
	bounds.left = x;
	bounds.top = y;
}


UIdeathCount::~UIdeathCount()
{

}

/**
 * Creat the board and draw it to the screen.
 * @param window
 */
void UIdeathCount::draw(sf::RenderWindow& window)
{
	Position pos1, pos2;

	//define some sizings
	int pieceRad = (bounds.width - 16) / 2;
	pos1.first = bounds.left + bounds.width / 2;
	pos2.first = pos1.first;
	pos1.second = bounds.top + 8 + pieceRad;
	pos2.second = pos1.second + pieceRad * 2 + 16;
	bounds.height = pos2.second + pieceRad + 8 - bounds.top;

	//background
	UIroundedRectangle background(bounds.width, bounds.height);
	background.setPosition(bounds.left, bounds.top);
	background.setRadius(8);
	background.setColor(sf::Color(0, 0, 0));  //black
	background.draw(window);

	//p1 piece and count
	UIpiece p1Piece(Piece(P_ONE), pos1, pieceRad);
	if (currentTurn == P_ONE) p1Piece.setState(UIpiece::SELECTED);
	p1Piece.draw(window);

	UIcenteredText p1Txt(bounds.width, 30);
	p1Txt.setPositionCentered(pos1.first, pos1.second);
	p1Txt.setString(to_string(p1Deaths));
	p1Txt.setCharacterSize(24);
	p1Txt.setCharacterStyle(sf::Text::Bold);
	p1Txt.draw(window);

	//p2 piece and count
	UIpiece p2Piece(Piece(P_TWO), pos2, pieceRad);
	if (currentTurn == P_TWO) p2Piece.setState(UIpiece::SELECTED);
	p2Piece.draw(window);

	UIcenteredText p2Txt(bounds.width, 30);
	p2Txt.setPositionCentered(pos2.first, pos2.second);
	p2Txt.setString(to_string(p2Deaths));
	p2Txt.setCharacterSize(24);
	p2Txt.setCharacterStyle(sf::Text::Bold);
	p2Txt.draw(window);
}

/**
 * Check whether the given position is within the board.
 * @param pos - the position to check.
 * @return the result of the check.
 */
bool UIdeathCount::containsPosition(Position pos)
{
	return bounds.contains(pos.first, pos.second);
}

/**
 * Get the board's bounding rectangle.
 * @return The bounding rectangle.
 */
sf::FloatRect UIdeathCount::getBounds()
{
	return bounds;
}
