/**************************************************************************//**
 * @file
 * @brief Implement the UI class.
 *****************************************************************************/
#include "ui.h"

/**
 * Constructor for the UI class.
 */
UI::UI() {
    window.create(sf::VideoMode(850, 800), "Manatee Deamon Checkers", sf::Style::Titlebar | sf::Style::Close);

    currentTurn = P_ONE;
    move_failed = false;

}

UI::~UI()
{
    clearUiElements();

    window.close();
}


/**
 * Initialize the UI and create UI objects.
 */
void UI::init()
{
	//load assets
	kingIconLoaded = kingIcon.loadFromFile("./Resources/kingIcon.png");

	//draw the UI
	thread(&UI::updaterLoop,this).detach();
}


/**
 * Asynchronously update the UI. Updates are performed by clearing the existing
 * UI components, creating new ones based on current data, and drawing the
 * components.
 */
void UI::updaterLoop()
{
    sf::Event event;

    typedef chrono::high_resolution_clock Time;
    chrono::milliseconds timeBetweenRefreshes = chrono::milliseconds(1000 / REFRESH_RATE);
    chrono::milliseconds timeLeft;
    auto lastRefresh = Time::now();

    while(true)
    {
		//get the window events since the last update
		while (window.pollEvent(event))
		{
			//handle clicks
			if (event.type == sf::Event::MouseButtonPressed &&
				event.mouseButton.button == sf::Mouse::Left)
			{
				handleClickEvent(event);
			}

			//close window
			if (event.type == sf::Event::Closed)
			{
				clearUiElements();

				window.close();
				exit(0);
			}
		}

		//pause to match our refresh rate
		timeLeft = chrono::duration_cast<std::chrono::milliseconds>(timeBetweenRefreshes - (Time::now() - lastRefresh));
		if (timeLeft > chrono::milliseconds::zero())
		{
			this_thread::sleep_for(timeLeft);
		}
		lastRefresh = Time::now();

		//update the display
		//only rebuild if state is dirty
		if (isDirty)
		{
			buildElements();
		}
		draw();

    }
}

/**
 * Iterate through the list of elements from the top down and call the handler
 * function for the first one that contains the click position.
 * @param event - the click event being handled.
 */
void UI::handleClickEvent(sf::Event event)
{
	Position mouse_pos = make_pair(event.mouseButton.x, event.mouseButton.y);

	for (auto i = uiElements.rbegin(); i != uiElements.rend(); ++i )
	{
		if ( (*i)->containsPosition(mouse_pos) )
		{
			(*i)->handleClick(mouse_pos);
			return;
		}
	}
}


/**
 * Create the UI elements.
 */
void UI::buildElements()
{
    UIelement* newElement;  //for constructing the UI elements
    Position pos;

    isDirty = false;

	clearUiElements();

	//calculate some sizing
    int windowWidth = window.getSize().x;
    int windowHeight = window.getSize().y;
	int boardSize = windowHeight - 40 - 16;
	int gridSize = boardSize / 8;
	int pieceRad = gridSize / 2 - 5;
	int moveRad = gridSize / 2;
	int buttonHeight = 55;
	int buttonWidth = buttonHeight;
	int deathCountWidth = 64;
	int popupWidth = 400;


	//Create and add the UI elements
	//board
	newElement = new UIboard(windowWidth - 20 - boardSize, 20, boardSize);
	if (currentMove.size() == 1)
		newElement->onClick([&](void){
			this->currentMove.pop_back();
			isDirty = true;
		});
	addUiElement(*newElement);
	boardBounds = ((UIboard*)newElement)->getBounds();

	//confirm button
	newElement = new UIbutton(buttonWidth, buttonHeight);
	((UIbutton*)newElement)->setPosition(15, 30);
	((UIbutton*)newElement)->setColor(sf::Color(0, 205, 90)); //green
	((UIbutton*)newElement)->setBorderColor(sf::Color(0, 130, 60));
	((UIbutton*)newElement)->setString(L"\u2713");
	((UIbutton*)newElement)->setCharacterSize(46);
	if (state == GETTING_MOVE && validMoves.getMoveOptions(Move(currentMove)).size() == 0)
	{
		newElement->onClick([&](void){
			this->state = MOVE_COMPLETE;
			isDirty = true;
		});
		setInfoText("Press the green confirm button to finish turn.");
	}
	else
	{
		((UIbutton*)newElement)->setDisabled();
	}
	addUiElement(*newElement);

	//undo button
	newElement = new UIbutton(buttonWidth, buttonHeight);
	((UIbutton*)newElement)->setPosition(15, 30 + 20 + buttonHeight);
	((UIbutton*)newElement)->setColor(sf::Color(255, 0, 0)); //red
	((UIbutton*)newElement)->setBorderColor(sf::Color(190, 0, 0));
	((UIbutton*)newElement)->setString(L"\u21B6");
	((UIbutton*)newElement)->setCharacterSize(56);
	if (state == GETTING_MOVE && currentMove.size() >= 1)
	{
		newElement->onClick([&](void){
			this->currentMove.pop_back();
			setInfoText();
			isDirty = true;
		});
	}
	else
	{
		((UIbutton*)newElement)->setDisabled();
	}
	addUiElement(*newElement);

	//moves
	if (currentMove.size() > 0)
	{
		for (auto& pos : validMoves.getMoveOptions(Move(currentMove)))
		{
			newElement = new UImove(gridToWinPos(pos), (double)moveRad);
			newElement->onClick([&, pos](void){
				this->currentMove.push_back(pos);
				isDirty = true;
			});
			addUiElement(*newElement);
		}
	}

    //info text
	newElement = new UIcenteredText(boardSize, 30);
	((UIcenteredText*)newElement)->setPositionCentered(windowWidth - 20 - boardSize / 2, windowHeight - 20);
	((UIcenteredText*)newElement)->setCharacterSize(24);
	((UIcenteredText*)newElement)->setString(infoText);
	((UIcenteredText*)newElement)->setCharacterStyle(sf::Text::Bold);
	addUiElement(*newElement);

	//pieces
	vector<Position> moveablePieces = validMoves.getMoveablePieces();
	for (auto & pieceData : boardData.return_pieces())
	{
		pos = pieceData.first;
		newElement = new UIpiece(pieceData.second, gridToWinPos(pos), (double)pieceRad);

		//display differently if moveable or being moved.
		if (currentMove.empty())
		{
			//check if moveable
			if (find(moveablePieces.begin(), moveablePieces.end(), pos) != moveablePieces.end())
			{
				newElement->onClick([&, pos](void){
					this->currentMove.push_back(Position(pos));
					isDirty = true;
				});
				((UIpiece*)newElement)->setState(UIpiece::SELECTABLE);
			}
		}
		else if (pos == currentMove.front()) //being moved
		{
			//move to updated position
			pos = currentMove.back();
			((UIpiece*)newElement)->setPosition(gridToWinPos(pos));

			((UIpiece*)newElement)->setState(UIpiece::SELECTED);
		}
		if (kingIconLoaded) ((UIpiece*)newElement)->setKingIcon(kingIcon);
		addUiElement(*newElement);
	}

	//death counts
	newElement = new UIdeathCount(deathCountWidth);
	((UIdeathCount*)newElement)->setPosition(11, windowHeight - (2 * deathCountWidth + 50));
	((UIdeathCount*)newElement)->setCurrentTurn(currentTurn);
	((UIdeathCount*)newElement)->setDeathCounts(p1Deaths, p2Deaths);
	addUiElement(*newElement);

	//Game result
	if (state == GAME_ENDED)
	{
		newElement = new UIgameEnd((windowWidth - popupWidth) / 2, windowHeight / 2 - 200, popupWidth);
		((UIgameEnd*)newElement)->setWinner(winningPlayer);
		((UIgameEnd*)newElement)->setMessage(gameEndMsg);
		newElement->onClick([&](void){
			this->state = EXIT;
			isDirty = true;
		});
		addUiElement(*newElement);
	}
}


/**
 * Convert a board grid position to a win7dow pixel position. Gives the position
 * of the center of the grid location.
 * @param pos - grid position (0-7)
 * @return the corresponding position on the screen
 */
Position UI::gridToWinPos(Position gridPos)
{
	Position windowPos;

	windowPos.first = boardBounds.left + ((gridPos.first - 0.5) * boardBounds.width / 8);
	windowPos.second = boardBounds.top + ((gridPos.second - 0.5) * boardBounds.height / 8);

	return windowPos;
}

/**
 * Get request a move from the UI. Puts the UI into the getting move state. The
 * process itself is defined through callbacks from UIelements. This function
 * waits for the process to complete and then returns the result.
 * @param b - the game board
 * @param validMoves - Collection of MoveTrees representing the move options.
 * @return the move from the user.
 */
Move UI::get_move(Board b, MoveTreeCollection validMoves)
{
	//initialize data
	update_board(b);
	update_validMoves(validMoves);
	currentMove = deque<Position>();

	//enter the get move state
	state = GETTING_MOVE;
	isDirty = true;

	//wait for the move to be constructed
	while(state == GETTING_MOVE)
	{
		this_thread::sleep_for(chrono::milliseconds(100));
	}

	state = NORMAL;
	isDirty = true;

	return Move(currentMove);
}

/**
 * Update the informational text. Will set to display the current player if no
 * text is passed.
 * @param txt - (optional) the text to set
 */
void UI::setInfoText(string txt)
{
	if (txt != "")
	{
		infoText = txt;
	}
	else
	{
		infoText = (currentTurn == P_ONE ? "Black's Turn" : "Red's Turn");
	}
	isDirty = true;
}


/**
 * Update the current turn in the UI.
 * @param id - id of the player who's turn it is.
 */
void UI::update_turn(PlayerID id)
{
	currentTurn = id;
	setInfoText();
	isDirty = true;
}

/**
 * Update the board used by the UI.
 * @param board - game board.
 */
void UI::update_board(Board& board)
{
	boardData = board;
	isDirty = true;
}

/**
 * Set the number of pieces each player has lost.
 * @param p1Deaths - number of pieces lost by p1 (black)
 * @param p2Deaths - number of pieces lost by p2 (red)
 */
void UI::update_death_count(int p1Deaths, int p2Deaths)
{
	this->p1Deaths = p1Deaths;
	this->p2Deaths = p2Deaths;
	isDirty = true;
}

/**
 * Update the MoveTree collection for the UI.
 * @param validMoves - MoveTreeCollection with moves for the current player.
 */
void UI::update_validMoves(MoveTreeCollection& validMoves)
{
	this->validMoves = validMoves;
	isDirty = true;
}

void UI::notify_invalid_move()
{
	setInfoText("Invalid move. Please try again.");
	isDirty = true;
}

/**
 * End the game. Show the winner or notify players of a draw along with the
 * reason.
 * @param winnerId - id of the winning player or NONE if game was a draw.
 * @param msg - message to explain the draw (optional).
 */
void UI::end_game(PlayerID winnerId, string msg)
{
	winningPlayer = winnerId;
	gameEndMsg = msg;

	this_thread::sleep_for(chrono::milliseconds(500));

	setInfoText("Game Over");
	state = GAME_ENDED;
	isDirty = true;

	while(state == GAME_ENDED)
	{
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}


/**
 * Add an element to the UI element list.
 * @param element the element to add.
 */
void UI::addUiElement(UIelement& element)
{
	uiElements.push_back(&element);
}


/**
 * Draw all UI elements.
 */
void UI::draw()
{
    window.clear(sf::Color::Blue);

    for( auto x : uiElements )
    {
        x->draw(window);
    }

    window.display();
}


/**
 * Destroy all of the UI's elements
 */
void UI::clearUiElements()
{
	for (auto& x : uiElements)
	{
		delete x;
	}
	uiElements.clear();
}
