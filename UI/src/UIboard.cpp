/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIboard class
 *****************************************************************************/

#include "UIboard.h"

/**
 * Constructor for the class.
 * @param left - Position of the left side.
 * @param top - Position of the top side.
 * @param size - width and height of the board.
 */
UIboard::UIboard(int left, int top, int size)
{
    grid_size = size / 8 ;

    bounds = sf::FloatRect(left, top, size, size);
}

UIboard::~UIboard()
{

}


/**
 * Creat the board and draw it to the screen.
 * @param window
 */
void UIboard::draw(sf::RenderWindow& window)
{
    bool colorBlack = false;  //for alternating square colors

	for (int y = 0; y < 8; y++)
	{
		for (int x = 0; x < 8; x++)
		{
			//initialize the square
			sf::RectangleShape boardSquare;

			//set size and position
			boardSquare.setSize(sf::Vector2f(grid_size, grid_size));
			boardSquare.setPosition(bounds.left + x * grid_size, bounds.top + y * grid_size);

			//set color
			boardSquare.setFillColor((colorBlack ? sf::Color::Black : sf::Color::Red));

			//set outline
			boardSquare.setOutlineThickness(2);
			boardSquare.setOutlineColor(sf::Color::Black);

	        //draw
	        window.draw(boardSquare);

			//alternate colors across row
			colorBlack = !colorBlack;
		}
		//start next row with a different color
		colorBlack = !colorBlack;
	}
}

/**
 * Check whether the given position is within the board.
 * @param pos - the position to check.
 * @return the result of the check.
 */
bool UIboard::containsPosition(Position pos)
{
	return bounds.contains(pos.first, pos.second);
}

/**
 * Get the board's bounding rectangle.
 * @return The bounding rectangle.
 */
sf::FloatRect UIboard::getBounds()
{
	return bounds;
}
