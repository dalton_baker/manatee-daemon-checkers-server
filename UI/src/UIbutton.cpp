/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIbuttons class
 *****************************************************************************/

#include <UIbutton.h>

/**************************************************************************//**
 * @author
 *
 * @par Description:
 * The default constructor for the UIbuttons class.
 *
 *****************************************************************************/
UIbutton::UIbutton(int width, int height)
{
	bounds = sf::FloatRect(0, 0, width, height);
}

UIbutton::UIbutton()
{
	bounds = sf::FloatRect(0, 0, 0, 0);
}

UIbutton::~UIbutton()
{
	if (strUnicode != nullptr)
		delete []strUnicode;
}

/**
 * Set the position for the element.
 * @param left - horizontal position.
 * @param top - vertical position.
 */
void UIbutton::setPosition(float left, float top)
{
	bounds.left = left;
	bounds.top = top;
}

/**
 * Set the character size for the text.
 * @param charSize - size in pixels.
 */
void UIbutton::setCharacterSize(int charSize)
{
	this->charSize = charSize;
}


/**
 * Set the position for the element using centered positioning. The middle of
 * the element will be at the given position.
 * @param x - horizontal position.
 * @param y - vertical position.
 */
void UIbutton::setPositionCentered(float x, float y)
{
	bounds.left = x - bounds.width / 2;
	bounds.top = y - bounds.height / 2;
}

/**
 * Set the button's text.
 * @param str - text for the button.
 */
void UIbutton::setString(string str)
{
	this->str = str;
}

/**
 * Set the button's text using a unicode string.
 * @param str - unicode text for the button.
 */
void UIbutton::setString(const wchar_t* str)
{
	//strUnicode = str;
	//wstring strUnicode(str);
	strUnicode = new wchar_t[wcslen(str)+1];
	wcscpy(strUnicode,str);
}

/**
 * Set the color for the button.
 * @param color - color of the button.
 */
void UIbutton::setColor(sf::Color color)
{
	this->color = color;
}

/**
 * Set the border color for the button.
 * @param color - color for the button's border.
 */
void UIbutton::setBorderColor(sf::Color color)
{
	this->borderColor = color;
}

/**
 * Set the state of the button to enabled. If disabled, the button will be
 * grayed out and unclickable.
 */
void UIbutton::setEnabled()
{
	enabled = true;
}

/**
 * Set the state of the button to disabled. If disabled, the button will be
 * grayed out and unclickable.
 */
void UIbutton::setDisabled()
{
	enabled = false;
}

/**
 * Create the button element and draw it to the window.
 * @param window - window for drawing.
 */
void UIbutton::draw(sf::RenderWindow& window)
{
    sf::RectangleShape button(sf::Vector2f(bounds.width, bounds.height));
    button.setPosition(bounds.left, bounds.top);
    button.setOutlineThickness(3);
    if (enabled)
    {
    	button.setFillColor(color);
    	button.setOutlineColor(borderColor);
    }
    else
    {
    	button.setFillColor(sf::Color(190, 190, 190));
    	button.setOutlineColor(sf::Color(125, 125, 125));
    }
    window.draw(button);

	UIcenteredText text(bounds.width, bounds.height);
	text.setPosition(bounds.left, bounds.top);
	if (strUnicode != nullptr)
	{
		text.setString(strUnicode);
		text.setCharacterStyle(sf::Text::Regular);
	}
	else
	{
		text.setString(str);
		text.setCharacterStyle(sf::Text::Bold);
	}
	text.setCharacterSize(charSize);
	text.draw(window);
}

/**
 * Check whether the given position is within the button.
 * @param pos - the position to check.
 * @return the result of the check.
 */
bool UIbutton::containsPosition(Position pos)
{
	return bounds.contains(pos.first, pos.second);
}


/**
 * Handle a click on the UI element.
 * @param pos - the position of the click.
 */
void UIbutton::handleClick(Position pos)
{
	if (clickCallback && enabled)
	{
		clickCallback(pos);
	}
}
