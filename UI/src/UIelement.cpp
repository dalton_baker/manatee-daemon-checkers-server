/**************************************************************************//**
 * @file
 * @brief This file contains definitions for some of the functions for the UIelement class
 *****************************************************************************/

#include "UIelement.h"


/**
 * Handle a click on the UI element.
 * @param pos - the position of the click.
 */
void UIelement::handleClick(Position pos)
{
	if (clickCallback)
	{
		clickCallback(pos);
	}
}

/**
 * Add a handler for click events.
 * @param callback - the function used to handle the events
 */
void UIelement::onClick(function<void()> callback)
{
	clickCallback = [callback](Position){callback();};
}

/**
 * Add a handler for click events. Handler has the position of the click as one
 * of its arguments.
 * @param callback - the function used to handle the events
 */
void UIelement::onClick(function<void(Position)> callback)
{
	clickCallback = callback;
}
