/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIboard class
 *****************************************************************************/

#include "UIgameEnd.h"

/**
 * Constructor for the class.
 * @param left - Position of the left side.
 * @param top - Position of the top side.
 * @param size - width and height of the board.
 */
UIgameEnd::UIgameEnd(int left, int top, int width)
{
	//use a placeholder value for height.
    bounds = sf::FloatRect(left, top, width, 1);
}

/**
 * Set the winning player.
 * @param winner - ID of the winning player.
 */
void UIgameEnd::setWinner(PlayerID winner)
{
	this->winner = winner;
}

/**
 * Set the message for the popup.
 * @param message - message to display with the result.
 */
void UIgameEnd::setMessage(string message)
{
	this->message = message;
}


UIgameEnd::~UIgameEnd()
{

}

/**
 * Creat the board and draw it to the screen.
 * @param window
 */
void UIgameEnd::draw(sf::RenderWindow& window)
{
	int yOffset = 20;  //offset from the top of the popup window
	int left = bounds.left + 20;
	int width = bounds.width - 40;
	Position pos;
	sf::Font font;
	font.loadFromFile("./UI/src/arial.ttf");

	//UI elements
	UIpiece plrPiece;
	UIroundedRectangle playerTxtBg;
	UIcenteredText playerTxt;


	//define some sizings
	int pieceRad = 45;
	int buttonHeight = 40;
	int buttonWidth = 100;


	//Heading
	UIcenteredText title(width, 50);
	title.setPosition(left, bounds.top + yOffset);
	if(winner == NONE)
	{
		title.setString("Draw");
	}
	else
	{
		title.setString("Winner!");
	}
	title.setCharacterSize(50);
	title.setCharacterStyle(sf::Text::Bold);

	yOffset += 50;

	if(winner != NONE)
	{
		//Piece and player name
		pos.first = left + width / 2;
		pos.second = bounds.top + yOffset + pieceRad;
		plrPiece = UIpiece(Piece(winner), pos, pieceRad);

		playerTxtBg = UIroundedRectangle(180, 46);
		playerTxtBg.setPositionCentered(pos.first, pos.second);
		playerTxtBg.setRadius(5);
		playerTxtBg.setColor(sf::Color(0, 0, 0, 150));  //black


		playerTxt = UIcenteredText(180, 36);
		playerTxt.setPositionCentered(pos.first, pos.second);
		playerTxt.setString("Player " + to_string(winner));
		playerTxt.setCharacterSize(30);
		playerTxt.setCharacterStyle(sf::Text::Bold);

		yOffset += pieceRad * 2;
	}

	//message
	pos.first = left + width / 2;
	pos.second = bounds.top + yOffset + 18;
	UIcenteredText gameMessage(width, 36);
	gameMessage.setPositionCentered(pos.first, pos.second);
	gameMessage.setString(message);
	gameMessage.setCharacterSize(20);
	gameMessage.setCharacterStyle(sf::Text::Bold);

	yOffset += 36 + 5;

	//button
	pos.first = left + width / 2;
	pos.second = bounds.top + yOffset + buttonHeight / 2;
	button = UIbutton(buttonWidth, buttonHeight);
	button.setPositionCentered(pos.first, pos.second);
	button.setColor(sf::Color(0, 175, 240));
	button.setBorderColor(sf::Color(0, 110, 190));
	button.setString("Exit");
	button.onClick([&](Position pos){this->clickCallback(pos);});  //use this class's click handler

	yOffset += buttonHeight;

	//window
	//adjust the window size if needed (so text doesn't overflow.)
	sf::FloatRect msgBounds = gameMessage.getBounds();
	if (bounds.left > msgBounds.left)
	{
		bounds.left = msgBounds.left - 20;
		bounds.width = msgBounds.width + 40;
	}
	bounds.height = yOffset + 20;
	UIroundedRectangle backing(bounds.width, bounds.height);
	backing.setPosition(bounds.left, bounds.top);
	backing.setRadius(30);
	backing.setBorderThickness(3);
	backing.setBorderColor(sf::Color::Black);
	backing.setColor(sf::Color(90, 155, 215, 225));  //light blue

	//draw components
	backing.draw(window);
	title.draw(window);
	if(winner != NONE)
	{
		plrPiece.draw(window);
		playerTxtBg.draw(window);
		playerTxt.draw(window);
	}
	gameMessage.draw(window);
	button.draw(window);
}

/**
 * Check whether the given position is within the board.
 * @param pos - the position to check.
 * @return the result of the check.
 */
bool UIgameEnd::containsPosition(Position pos)
{
	return bounds.contains(pos.first, pos.second);
}

/**
 * Get the board's bounding rectangle.
 * @return The bounding rectangle.
 */
sf::FloatRect UIgameEnd::getBounds()
{
	return bounds;
}

/**
 * Handle a click on the UI element. Check if the click is on the button and send
 * @param pos - the position of the click.
 */
void UIgameEnd::handleClick(Position pos)
{
	if (clickCallback)
	{
		if (button.containsPosition(pos))
		{
			button.handleClick(pos);
		}
	}
}
