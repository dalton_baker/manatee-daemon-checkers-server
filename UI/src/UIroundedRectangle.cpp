/**************************************************************************//**
 * @file
 * @brief This file contains all of the members of the UIroundedRectangle class
 *****************************************************************************/

#include "UIroundedRectangle.h"

/**
 * Constructor for the class.
 * @param width - height of the element.
 * @param height - height of the element.
 */
UIroundedRectangle::UIroundedRectangle(int width, int height)
{
	//use a placeholder value for height.
    bounds = sf::FloatRect(0, 0, width, height);
}

/**
 * Constructor for the class. Initialize to dummy values.
 */
UIroundedRectangle::UIroundedRectangle()
{
	//use placeholder values.
    bounds = sf::FloatRect(0, 0, 0, 0);
}

/**
 * Set the position for the element.
 * @param x - horizontal position.
 * @param y - vertical position.
 */
void UIroundedRectangle::setPosition(float x, float y)
{
	bounds.left = x;
	bounds.top = y;
}

/**
 * Set the position for the element using centered positioning. The middle of
 * the element will be at the given position.
 * @param x - horizontal position.
 * @param y - vertical position.
 */
void UIroundedRectangle::setPositionCentered(float x, float y)
{
	bounds.left = x - bounds.width / 2;
	bounds.top = y - bounds.height / 2;
}

/**
 * Set the corner radius for the rectangle.
 * @param radius - radius for the rectangle.
 */
void UIroundedRectangle::setRadius(int radius)
{
	this->radius = radius;
}

/**
 * Set the color of the rectangle.
 * @param color - color for the rectangle.
 */
void UIroundedRectangle::setColor(sf::Color color)
{
	this->color = color;
}

/**
 * Set the color of the border.
 * @param borderColor - color for the border.
 */
void UIroundedRectangle::setBorderColor(sf::Color borderColor)
{
	this->borderColor = borderColor;
}

/**
 * Set the thickness of the border.
 * @param borderThickness - thickness of the border.
 */
void UIroundedRectangle::setBorderThickness(int borderThickness)
{
	this->borderThickness = borderThickness;
}


UIroundedRectangle::~UIroundedRectangle()
{

}

/**
 * Create the rectangle and draw it to the screen.
 * @param window - window to draw to.
 */
void UIroundedRectangle::draw(sf::RenderWindow& window)
{
	sf::ConvexShape rrect;

	rrect.setFillColor(color);
	rrect.setOutlineThickness(borderThickness);
	rrect.setOutlineColor(borderColor);

	//create points
	rrect.setPointCount((radius + 1) * 4);
	addRadiusPoints(rrect, bounds.left, bounds.top, 0);
	addRadiusPoints(rrect, bounds.left, bounds.top + bounds.height, 1);
	addRadiusPoints(rrect, bounds.left + bounds.width, bounds.top + bounds.height, 2);
	addRadiusPoints(rrect, bounds.left + bounds.width, bounds.top, 3);

	//draw
	window.draw(rrect);
}


/**
 * Add points to the shape being constructed for a single corner. Corners are
 * defined in CCW order starting at top left.
 * @param rrect - the shape being constructed.
 * @param cornerX - the x coordinate of the corner
 * @param cornerY - the y coordinate of the corner
 * @param cornerNum - number of the corner (0-3)
 * @param radius - radius of the corner
 */
void UIroundedRectangle::addRadiusPoints(sf::ConvexShape& rrect, float cornerX,
		float cornerY, int cornerNum)
{
	static int pointCount = 0;
	float centerX, centerY, startAngle;
	float angle, x, y; //iteration values
	int numPoints = radius;

	switch (cornerNum) {
		case 1: //bottom left
			startAngle = 0;
			centerX = cornerX + radius;
			centerY = cornerY - radius;
			break;
		case 2: //bottom right
			startAngle = M_PI/2;
			centerX = cornerX - radius;
			centerY = cornerY - radius;
			break;
		case 3: //top right
			startAngle = M_PI;
			centerX = cornerX - radius;
			centerY = cornerY + radius;
			break;
		default: //top left
			startAngle = 3*M_PI/2;
			centerX = cornerX + radius;
			centerY = cornerY + radius;
			pointCount = 0;
	}

	//draw the points
	for(int i=0; i<=numPoints; i++)
	{
		angle = startAngle + (M_PI/2) / numPoints * i;
		x = centerX - cos(angle) * radius;
		y = centerY + sin(angle) * radius;
		rrect.setPoint(pointCount, sf::Vector2f(x,y));
		pointCount++;
	}
}

/**
 * Check whether the given position is within the element.
 * @param pos - the position to check.
 * @return the result of the check.
 */
bool UIroundedRectangle::containsPosition(Position pos)
{
	return bounds.contains(pos.first, pos.second);
}
