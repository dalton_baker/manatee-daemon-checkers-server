/**************************************************************************//**
 * @file
 * @brief Start of the program, only contains main.
 ****************************************************************************/

#include "game.h"
#include "playerLoader.h"
#include <string.h>

using namespace std;

int main(int argc, char** argv)
{
    UI* ui              = nullptr;
    bool ui_output      = true;
    bool console_output = false;
    bool noDelay        = false;
    string playerDirectory = ".";

    if (const char *env_p = std::getenv("CHECKER_PLAYERDIR"))
        playerDirectory = env_p;

    PlayerLoader playerLoader;
    Player* red = nullptr;
    Player* black = nullptr;

    int i;

    playerLoader.loadPlayers(playerDirectory);
 
    for ( i = 1; i < argc; i++ )
    {
        if ( strcmp(argv[i], "-r") == 0 && i+1 < argc)
        {
           red = playerLoader.getPlayerObject(argv[i+1]);
           if (red == nullptr)
           {
              cout << "Cannot load red player >" << argv[i+1] << "<" << endl;
              return -1;
           }
        }
        if ( strcmp(argv[i], "-b") == 0 && i+1 < argc)
        {
            black = playerLoader.getPlayerObject(argv[i+1]);
            if (black == nullptr)
            { 
              cout << "Cannot load black player >" << argv[i+1] << "<" << endl;
              return -1;
           }
        }
        if ( strcmp(argv[i], "-g") == 0 )
            ui_output = false;
        if ( strcmp(argv[i], "-c") == 0 )
            console_output = true;
    }

    if (!console_output && !ui_output)
       noDelay = true;

    if ( !red || !black )
    {
        // If a human player exists and the UI is off, turn console output on automatically
        if ( !ui_output )
            console_output = true;
    }

    if ( ui_output )
    {
        ui = new UI();
        //display welcome and set things up
        ui->init();
    }

    if ( !red )
    {
        Player *p = playerLoader.getPlayerObject("HumanPlayer");
        red = p;
    }

    if ( !black )
    {
        Player *p = playerLoader.getPlayerObject("HumanPlayer");
        black = p;
    }

    Game game(black, red, ui, console_output);
    game.run(noDelay);

    if ( ui != nullptr )
        delete ui;

    return 0;
}
