#!/usr/bin/env bash

# This script runs the program a series of times in repetition.

function usage() {
    cat << EOF
run.sh runs a number of games and stores the results.
Example: \$./run.sh 10
Usage: $0 [-h] [-r] [-s] [-c] [-p PATH] [-d DATAFILE] [-e ERRFILE] [-f LOGFILE] [-a "ARGS"] iterations
where:
    iterations is the number of runs to perform
    -a "ARGS" sends ARGS to the executable (quotes necessary for multiple args, "-r -b -g" by default)
    -c does "make clean" before and after
    -d DATAFILE adds the program's win/loss stats onto DATAFILE (data.dat by default)
    -e ERRFILE adds the program's errors onto ERRFILE (error_log.txt by default)
    -f LOGFILE adds the program's output onto LOGFILE (log.txt by default)
    -p PATH uses PATH as the executable (../bin/mdcheckers by default)
    -r clear the log and data files before starting
    -s silences status messages
    -h prints this message
EOF
    exit 0
}

# Print usage if no arguments were given.
if [[ "$#" -eq "0" ]]; then
    usage
fi

# variable declarations
script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P )
args="-r -b -g"
clean="F"
datafile="$script_dir/data.dat"
iterations=${!#}
errorfile="$script_dir/error_log.txt"
logfile="$script_dir/log.txt"
tempfile=$(mktemp)
remove="F"
verbose="-v"

# move to Checkers folder for make to work properly
cd "$script_dir/.."
executable="./bin/mdcheckers"

# Parse commandline arguments.
while getopts "a:cd:e:f:p:rsh" op
do
    case $op in
        a) args="$OPTARG" ;;
        c) clean="T" ;;
        d) datafile="$OPTARG" ;;
        e) errorfile="$OPTARG" ;;
        f) logfile="$OPTARG" ;;
        p) executable="$OPTARG" ;;
        r) remove="T" ;;
        s) verbose="" ;;
        h) usage ;;
        \?) usage ;;
        *) usage ;;
    esac
done

# Check that iterations was given,
# e.g., the last commandline argument is not -c or -v,
# and the second to last argument is not -a, -f, or -p.
if [[ "$iterations" == "-c" || "$iterations" == "-v" ||
       "${@: -2:1}" == "-a" ||  "${@: -2:1}" == "-d" || "${@: -2:1}" == "-e" ||
       "${@: -2:1}" == "-f" ||  "${@: -2:1}" == "-p" ]]
then
    echo "Iterations is a required argument."
    usage
fi

if [[ "$remove" == "T" ]]; then
    > "$logfile"
    > "$datafile"
fi

# Do make clean, redirects stdout to /dev/null.
# Status messages are controlled by <verbose>
function do_make() {
    if [[ "$verbose" == "-v" ]]; then
        printf "Making ... "
    fi

    make 1> /dev/null

    if [[ "$verbose" == "-v" ]]; then
        printf "make done.\n"
    fi
}

# Do make clean if ("$1" == "T"),
#   redirects stdout to /dev/null and sterr to <errorfile>
# Status messages are controlled by <verbose>
# Usage: do_make_clean "T"
function do_make_clean() {
    if [[ "$1" == "T" ]]; then
        if [[ "$verbose" == "-v" ]]; then
            printf "Making clean ... "
        fi

        make clean > /dev/null 2>> "$errorfile"

        if [[ "$verbose" == "-v" ]]; then
            printf "make clean done.\n"
        fi
    fi
}


# Do make clean if -c was given.
do_make_clean "$clean"

# Run make.
do_make

if [[ "$verbose" == "-v" ]]; then
    printf "Running ..."
fi

# Run <executable> <iterations> number of times, redirecting stdout to
#   <logfile> and stderr to <errorfile>.
# Print progress between runs if -v was given.
function run {
    local status

    for (( i = 1; i <= iterations; ++i )); do
        status=$($executable $args >> "$tempfile" 2>> "$errorfile")

        if [[ "$status" -ne "0" && "$verbose" == "-v" ]]; then
            echo "Run had non-zero exit status: $status."
        fi

        if [[ "$verbose" == "-v" ]]; then
            printf "\rRun %4s out of %s completed." "$i" "$iterations"
        fi
    done
    echo

    local -i p1_wins_by_capture=$(cat "$tempfile" | grep -P\
                '^Player one wins!$' | wc -l)
    local -i p2_wins_by_capture=$(cat "$tempfile" | grep -P\
                '^Player two wins!$' | wc -l)
    local -i p1_wins_by_pinning=$(cat "$tempfile" | grep -P\
                '^Player one wins! Player two cannot move.$' | wc -l)
    local -i p2_wins_by_pinning=$(cat "$tempfile" | grep -P\
                '^Player two wins! Player one cannot move.$' | wc -l)
    local -i draws_by_stalling=$(cat "$tempfile" | grep -P\
                '^Draw: fifty moves since last capture or kinging.$' | wc -l)
    local -i draws_by_repetition=$(cat "$tempfile" | grep -P\
                '^Draw: this board state has occurred twice before.$' | wc -l)

    cat "$tempfile" >> "$logfile"
    rm "$tempfile"

    # if <datafile> DNE or is empty print the data's formatting
    if [[ ! -s "$datafile" ]]; then
        printf "%19s |%19s |%19s |%19s |%19s |%19s\n"\
               "p1 wins by capture"\
               "p2 wins by capture"\
               "p1 wins by pinning"\
               "p2 wins by pinning"\
               "draws by stalling"\
               "draws by repetition" >> "$datafile"
    fi

    # print stats seperated by spaces for easy cut parsing
    local sep='         |'
    printf "%11d%s%11d%s%11d%s%11d%s%11d%s%11d\n"\
           "$p1_wins_by_capture" "$sep"\
           "$p2_wins_by_capture" "$sep"\
           "$p1_wins_by_pinning" "$sep"\
           "$p2_wins_by_pinning" "$sep"\
           "$draws_by_stalling"  "$sep"\
           "$draws_by_repetition" >> "$datafile"

   if [[ "$verbose" == "-v" ]]; then
       echo
       echo "Player 1 won $((p1_wins_by_capture + p1_wins_by_pinning)) games."
       echo "Player 2 won $((p2_wins_by_capture + p2_wins_by_pinning)) games."
       echo "Players tied $((draws_by_stalling + draws_by_repetition)) times."
   fi
}

run

# Do make clean if -c was given.
do_make_clean "$clean"

if [[ "$verbose" == "-v" ]]; then
    echo
    echo "Detailed win/loss stats can be found on the last line of $(basename $datafile)."
    echo "Run output can be found in $(basename $logfile)."
    echo "Run errors can be found in $(basename $errorfile)."
fi

exit 0
