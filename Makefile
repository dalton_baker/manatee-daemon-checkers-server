GCC = g++
CFLAGS = -Wall -std=c++11 -O3 -g
SFMLFLAGS = -lsfml-graphics -lsfml-window -lsfml-system

ENGINE_PATH = Engine
PLAYER_PATH = Players
UI_PATH = UI

SRC_DIR = src
BIN_DIR = bin
INC_DIR = inc
GLOBAL_INC = inc

EXE_NAME = mdcheckers

INCLUDE_FLAGS = -I$(ENGINE_PATH)/$(INC_DIR) -I$(UI_PATH)/$(INC_DIR) -I$(GLOBAL_INC) -pthread

ENGINE_SRC = $(shell find $(ENGINE_PATH)/$(SRC_DIR) -name '*.cpp')
ENGINE_BIN = $(subst $(SRC_DIR),$(BIN_DIR),$(ENGINE_SRC:.cpp=.o))
PLAYER_BIN = $(subst $(SRC_DIR),$(BIN_DIR),$(PLAYER_SRC:.cpp=.o))
UI_SRC = $(shell find $(UI_PATH)/$(SRC_DIR) -name '*.cpp')
UI_BIN = $(subst $(SRC_DIR),$(BIN_DIR),$(UI_SRC:.cpp=.o))
BIN = $(PLAYER_BIN)

LIBFLAGS = -LEngine/bin -LPlayers -LUI/bin

LIBEND = -lengine -lUI

all: $(BIN_DIR)/$(EXE_NAME)

$(BIN_DIR)/$(EXE_NAME): $(ENGINE_BIN) $(BIN_DIR)/main.o $(BIN) $(UI_BIN) $(RED_LIB_NAME) $(BLACK_LIB_NAME)
	$(GCC) $(LIBFLAGS) -o $(BIN_DIR)/$(EXE_NAME) $(CFLAGS) $(INCLUDE_FLAGS) $(BIN) $(BIN_DIR)/main.o $(LIBEND) $(SFMLFLAGS)

$(ENGINE_BIN): $(ENGINE_SRC)
	(cd $(ENGINE_PATH); make all)

$(PLAYER_BIN): $(PLAYER_SRC)
	(cd $(PLAYER_PATH); make all)

$(UI_BIN): $(UI_SRC)
	(cd $(UI_PATH); make all)

$(BIN_DIR)/main.o: $(BIN_DIR)
	$(GCC) -c -o $(BIN_DIR)/main.o main.cpp $(CFLAGS) $(INCLUDE_FLAGS)

$(BIN_DIR):
	mkdir $(BIN_DIR)

clean_players:
	(cd $(PLAYER_PATH); make clean)

clean:
	(cd $(ENGINE_PATH); make clean)
	(cd $(PLAYER_PATH); make clean)
	(cd $(UI_PATH); make clean)
	rm -rf $(BIN_DIR)
