/**************************************************************************//**
 * @file
 * @brief This is the header for the gameManager.cpp file
 *****************************************************************************/
#ifndef __UTILITIES__H__
#define __UTILITIES__H__

#include <queue>
#include <map>
#include <iostream>
#include "piece.h"

using namespace std;

/*<! This is the form a position on the board will take*/
typedef pair<int, int> Position;
/*<! The type for storing move sequences*/
typedef queue <Position> Move;
/*<! This is the form sets of pieces in the board will take*/
typedef map<Position, Piece> PieceMap;

ostream& operator<<(ostream& out, const Position& pos);
bool operator<(const Position lhs, const Position rhs);
ostream& operator<<(ostream& out, Move move);
bool operator==(const Move lhs, const Move rhs);

#endif
