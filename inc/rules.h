/**************************************************************************//**
 * @file
 * @brief This is the header for the gameManager.cpp file
 *****************************************************************************/
#ifndef __RULES__H__
#define __RULES__H__

#include "board.h"
#include "moveTreeCollection.h"
#include <vector>

/*!
 * @brief GameManager documantation
 */
class Rules
{
protected:
    vector< pair<Move, Board> > possible_moves;
    PieceMap curr_pieces;
    Board curr_board;
    PlayerID curr_player;

    void find_reg_moves();
    void find_jump_moves();
    void find_next_jump(Position p, bool is_king, Move m, Board b);
    void find_king(Position p, Board &b);
    vector<Position> get_diag_loc(Position p, int dist);

public:
    Rules(Board curr_state, PlayerID player_id);

    bool find_move( Move move ) const;
    Board get_board( Move move ) const;
    int return_num_moves() const;
    MoveTreeCollection return_move_tree() const;
    vector<Move> return_move_vect() const;
    Rules get_next_rules( Move m ) const;
};

#endif
