/**************************************************************************//**
 * @file
 * @brief This is the header for the UI.cpp file
 *****************************************************************************/
#ifndef __UI__H__
#define __UI__H__

#define _BCXX_USE_NANOSLEEP

#include "utilities.h"
#include "board.h"
#include "moveTreeCollection.h"
#include <cstring>
#include <string>
#include <thread>
#include <future>
#include <functional>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>



#include "../UI/inc/UIboard.h"
#include "../UI/inc/UIbutton.h"
#include "../UI/inc/UImove.h"
#include "../UI/inc/UIpiece.h"
#include "../UI/inc/UIcenteredText.h"
#include "../UI/inc/UIroundedRectangle.h"
#include "../UI/inc/UIgameEnd.h"
#include "../UI/inc/UIdeathCount.h"



class UI
{
public:
	const int REFRESH_RATE = 20;
	enum State {NORMAL, GETTING_MOVE, MOVE_COMPLETE, GAME_ENDED, EXIT};

	UI();
	~UI();

	void init();
	Move get_move(Board, MoveTreeCollection);
	void update_turn(PlayerID);
	void update_board(Board&);
	void notify_invalid_move();
	void update_death_count(int, int);
	void end_game(PlayerID id, string msg = "");
	void draw();
	Position gridToWinPos(Position pos);
	Position get_clicks();
	void set_move_fail(bool);
	void set_curr_player(PlayerID);

private:

	sf::RenderWindow window;
	PlayerID currentTurn;
	bool move_failed;
	vector<UIelement*> uiElements;
	Board boardData;
	deque<Position> currentMove;
	MoveTreeCollection validMoves;
	sf::FloatRect boardBounds;
	State state = NORMAL;
	PlayerID winningPlayer = NONE;
	string gameEndMsg;
	int p1Deaths = 0;
	int p2Deaths = 0;
	string infoText = "";
    sf::Texture kingIcon;
    bool kingIconLoaded = false;
    bool isDirty = true;  //track whether the UI needs rebuild

	void addUiElement(UIelement& element);
	void clearUiElements();
	void buildElements();
	//void update_move(Move);
	void update_validMoves(MoveTreeCollection&);
	void updaterLoop();
	void handleClickEvent(sf::Event);
	void setInfoText(string txt = "");
};

#endif
