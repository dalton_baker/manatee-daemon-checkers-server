/**************************************************************************//**
 * @file
 * @brief This is the header for the piece.cpp file
 *****************************************************************************/
#ifndef __PIECE__H__
#define __PIECE__H__

#include <iostream>

enum PlayerID : unsigned char { NONE, P_ONE, P_TWO };

using namespace std;

class Piece
{
protected:
    PlayerID player_id;
    bool king;

public:
    Piece(PlayerID p_id = NONE);

    bool is_player1() const;
    bool is_player2() const;
    bool is_king() const;
    void king_piece();

    bool operator==(const Piece& other) const;
    bool operator<(const Piece& other) const;
    friend ostream& operator<<(ostream& out, const Piece& p);
};

#endif
