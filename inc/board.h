/**************************************************************************//**
 * @file
 * @brief This is the header for the board.cpp file
 *****************************************************************************/
#ifndef __BOARD__H__
#define __BOARD__H__

#include "utilities.h"
#include <iostream>

using namespace std;

/*!
 * @brief Board stores the board's current state
 *        and supplies methods for safely mutating that state.
 */
class Board
{
protected:
    PieceMap the_board;

public:
    Board();
    Board(const Board& b);

    bool move_piece(Position start, Position end);
    bool kill_piece(Position p);
    bool king_piece(Position p);
    const PieceMap player1_pieces() const;
    const PieceMap player2_pieces() const;
    const PieceMap return_pieces() const;
    bool is_player1(Position p) const;
    bool is_player2(Position p) const;
    bool is_king(Position p) const;
    bool is_empty(Position p) const;


    bool operator==(const Board& other) const;
    bool operator<(const Board& other) const;
    friend ostream& operator<<(ostream& out, const Board& board);
};

#endif
