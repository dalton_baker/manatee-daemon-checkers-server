/**************************************************************************//**
 * @file
 * @brief This is the header for the moveTreeCollection.cpp file
 *****************************************************************************/
#ifndef __MOVETREECOLLECTION__H__
#define __MOVETREECOLLECTION__H__

#include "utilities.h"
#include "moveTree.h"

using namespace std;

/**
 * A collection of MoveTrees. Each tree is rooted at the starting position of
 * a piece. This class can add trees for pieces, check for existence, and get trees.
 */
class MoveTreeCollection
{
protected:
	map<Position, MoveTree> treeCollection;

public:
	MoveTreeCollection();

    void addTree(MoveTree& tree);
    void addTree(Position pos);
    bool hasTree(Position pos);
    MoveTree& getTree(Position pos);
    void insert(Move m);
    vector<Position> getMoveOptions(Move m);
    vector<Position> getMoveablePieces();
    bool moveIsValid(Move m);

    typedef map<Position, MoveTree>::iterator iterator;
    typedef map<Position, MoveTree>::const_iterator const_iterator;
    iterator begin();
    iterator end();
};

#endif
