/**************************************************************************//**
 * @file
 * @brief This is the header for the moveTree.cpp file
 *****************************************************************************/
#ifndef __MOVETREE__H__
#define __MOVETREE__H__

#include "utilities.h"

using namespace std;

/**
 * Class to represent all the move options for a particular piece. Each node of
 * the tree represents a position on the board, and the root node is the starting
 * position.
 */
class MoveTree
{
protected:
    map<Position, MoveTree> children;
    Position pos;

public:
    MoveTree();
    MoveTree(Position pos);

    void addChild(Position p);
    MoveTree& getChild(Position p);
    vector<Position> getChildPositions();
    bool hasChildren();
    bool hasChild(Position pos);
    Position getPos();
    void insert(Move m);
    vector<Position> getMoveOptions(Move m);
    bool moveIsValid(Move m);
};

#endif
