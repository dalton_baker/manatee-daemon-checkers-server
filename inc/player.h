#ifndef __PLAYER__H__
#define __PLAYER__H__

#include "board.h"
#include "rules.h"
#include <vector>

class Player
{
protected:
   string name;
public:
    virtual Move get_move(Board, Rules)    = 0;
    virtual ~Player() {};
    string Name() { return name ; }
};

#endif
